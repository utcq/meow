#!/bin/bash

SOURCE_DIR=$1
OUTPUT_FILE=$2

if [ -z "$SOURCE_DIR" ] || [ -z "$OUTPUT_FILE" ]; then
    echo "Usage: $0 <source_directory> <output_file>"
    exit 1
fi

if [ ! -d "$SOURCE_DIR" ]; then
    echo "Source directory does not exist."
    exit 1
fi

TEMP_FILE=$(mktemp)

write_uint32_le() {
    local num=$1
    printf "%08x" "$num" | xxd -r -p >> "$TEMP_FILE"
}

write_uint64_le() {
    local num=$1
    printf "%016x" "$num" | xxd -r -p >> "$TEMP_FILE"
}

write_uint8() {
    local num=$1
    printf "%02x" "$num" | xxd -r -p >> "$TEMP_FILE"
}

get_entry_type() {
    local entry=$1
    if [ -d "$entry" ]; then
        echo "D" >> "$TEMP_FILE"  # Directory
    else
        echo "F" >> "$TEMP_FILE"  # Regular file
    fi
}

echo -n "IRFS" > "$TEMP_FILE"

find "$SOURCE_DIR" -type d -printf "%P\n" | while read -r DIR; do
    ENTRY_PATH="$SOURCE_DIR/$DIR"
    ENTRY_TYPE=$(get_entry_type "$ENTRY_PATH")
    ENTRY_SIZE=$(( ${#DIR} + 2 ))
    write_uint32_le "$ENTRY_SIZE"
    write_uint64_le 0
    echo -n "/$DIR" >> "$TEMP_FILE"
    write_uint8 0
done

find "$SOURCE_DIR" -type f -printf "%P\n" | while read -r FILE; do
    FILE_PATH="$SOURCE_DIR/$FILE"
    ENTRY_TYPE=$(get_entry_type "$FILE_PATH")
    FILE_SIZE=$(stat --format="%s" "$FILE_PATH")
    PATH_SIZE=$(( ${#FILE} + 2 ))
    write_uint32_le "$PATH_SIZE"
    write_uint64_le "$FILE_SIZE"
    echo -n "/$FILE" >> "$TEMP_FILE"
    write_uint8 0
    cat "$FILE_PATH" >> "$TEMP_FILE"
done

gzip -n -c "$TEMP_FILE" > "$OUTPUT_FILE"
#cp "$TEMP_FILE" "$OUTPUT_FILE"
rm "$TEMP_FILE"
