VERSION := 0.0.1

KERNEL_D := src/kernel
KTARGET := $(KERNEL_D)/kernel.00
IMAGE := meow_build_v$(VERSION).iso
ISODIR := boot/iso_root
LIMINE_CFG := boot/limine.cfg
LIMINE := misc/limine
RD_DIR := initrd
RD_ARCH := $(ISODIR)/deps/initrd.cpio.gz

KDEPS := $(shell find $(KERNEL_D)/src/ -type f -name '*.c' -o -name '*.S')
RDDEPS := $(shell find $(RD_DIR)/ -type f)

QEMU_ARGS := -smp 2

$(if $(ARCH),,$(error cpu architecture is not defined))

ifeq ($(ARCH), aarch64)
	GDB := aarch64-linux-gnu-gdb
	QEMU_ARGS += -M virt-9.0
else
	GDB := gdb
endif

all: $(LIMINE) $(IMAGE)

$(KTARGET): $(KDEPS)
	make -C $(KERNEL_D)

$(IMAGE): $(KTARGET) $(RD_ARCH)
	mkdir -p $(ISODIR)/boot/limine
	mkdir -p $(ISODIR)/EFI/BOOT
	cp $(KTARGET) $(ISODIR)/boot/meow.kernel
	cp -v $(LIMINE_CFG) $(LIMINE)/limine-bios.sys $(LIMINE)/limine-bios-cd.bin \
      $(LIMINE)/limine-uefi-cd.bin $(ISODIR)/boot/limine/
	cp -v $(LIMINE)/BOOTX64.EFI $(LIMINE)/BOOTIA32.EFI $(ISODIR)/EFI/BOOT/
	xorriso -as mkisofs -b boot/limine/limine-bios-cd.bin \
        -no-emul-boot -boot-load-size 4 -boot-info-table \
        --efi-boot boot/limine/limine-uefi-cd.bin \
        -efi-boot-part --efi-boot-image --protective-msdos-label \
        $(ISODIR) -o $@
	$(LIMINE)/limine bios-install $@

run: $(IMAGE)
	qemu-system-$(ARCH) $(QEMU_ARGS) -cdrom $(IMAGE) -serial stdio --no-reboot --no-shutdown

runint: $(IMAGE)
	qemu-system-$(ARCH) $(QEMU_ARGS) -cdrom $(IMAGE) -serial stdio --no-reboot --no-shutdown -d int	

debug: $(IMAGE)
	qemu-system-$(ARCH) -s -S -cdrom $(IMAGE) -serial stdio --no-reboot --no-shutdown -d int
gdb:
	sudo $(GDB) -ex "target remote localhost:1234" -ex "symbol-file $(KTARGET)" -ex "b _start" -ex "c" -ex "layout src" -ex "layout split"

initrd: $(RD_ARCH)

$(RD_ARCH): $(RDDEPS)
	mkdir -p $(shell dirname $(RD_ARCH))
	misc/initramfs/irfs_helper.sh $(RD_DIR) $(RD_ARCH)

$(LIMINE):
	bash misc/tools/bootstrap.sh

.PHONY: clean
clean:
	make -C $(KERNEL_D) clean
	rm $(IMAGE)
	rm $(RD_ARCH)
