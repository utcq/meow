PREFIX := i386

LINKERSCRIPT := ../kernel/src/arch/$(PREFIX)/linker.ld

KCFLAGS += -Wall \
	-m32 \
	-Wextra \
	-std=gnu11 \
	-ffreestanding \
	-fno-stack-protector \
	-fno-stack-check \
	-fno-lto \
	-mno-80387 \
	-mno-mmx \
	-mno-sse \
	-mno-sse2 \
	-mno-red-zone \

KDLDFLAGS += -m elf_i386 \
	-nostdlib \
	-z text \
	-z max-page-size=0x1000 \
	-T $(LINKERSCRIPT)

ALL_CFILES := $(shell find src/ -type f -name '*.c')
ALL_ASFILES := $(shell find src/ -type f -name '*.S')
CFILES := $(shell mkarch/path_helper.sh "$(ALL_CFILES)" "$(PREFIX)")
ASFILES := $(shell mkarch/path_helper.sh "$(ALL_ASFILES)" "$(PREFIX)")
