#include <libm/types.h>

#ifndef DRIVER_GFX_H
#define DRIVER_GFX_H

struct gfx_window {
    u16 width;
    u16 height;
    u16 x;
    u16 y;
};

void gfx_draw_pixel(struct gfx_window *window, u16 x, u16 y, u32 color);
void gfx_draw_line(struct gfx_window *window, u16 x1, u16 y1, u16 x2, u16 y2, u32 color);
void gfx_draw_rect(struct gfx_window *window, u16 x, u16 y, u16 width, u16 height, u32 color);
void gfx_draw_filled_rect(struct gfx_window *window, u16 x, u16 y, u16 width, u16 height, u32 color);
struct gfx_window *gfx_new_window(u16 width, u16 height, u16 x, u16 y);
struct gfx_window *gfx_screen_info();
void setup_gfx();

#endif