#include <libm/types.h>
#include <drivers/gfx/gfx.h>
#include <drivers/framebuffer/fb.h>
#include <mem/heap/heap.h>
#include <logger/log.h>
#include <libm/assert.h>
#include <disk/ramdisk.h>

extern void *memcpy(void *dest, const void *src, size_t n);

#include <checkpoint/checkpoint.h>

void gfx_draw_pixel(struct gfx_window *window, u16 x, u16 y, u32 color) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    if (x >= window->width || y >= window->height) {
        CLEAR_CHECKPOINT();
        return;
    }
    fb_pixel(window->x + x, window->y + y, color);
    CLEAR_CHECKPOINT();
}

void gfx_draw_line(struct gfx_window *window, u16 x1, u16 y1, u16 x2, u16 y2, u32 color) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    fb_line(window->x + x1, window->y + y1, window->x + x2, window->y + y2, color);
    CLEAR_CHECKPOINT();
}

void gfx_draw_rect(struct gfx_window *window, u16 x, u16 y, u16 width, u16 height, u32 color) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    fb_rect(window->x + x, window->y + y, width, height, color);
    CLEAR_CHECKPOINT();
}

void gfx_draw_filled_rect(struct gfx_window *window, u16 x, u16 y, u16 width, u16 height, u32 color) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    fb_fill_rect(window->x + x, window->y + y, width, height, color);
    CLEAR_CHECKPOINT();
}

struct gfx_window *gfx_new_window(u16 width, u16 height, u16 x, u16 y) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    struct gfx_window *window = kmalloc(sizeof(struct gfx_window));
    window->width = width;
    window->height = height;
    window->x = x;
    window->y = y;
    CLEAR_CHECKPOINT();
    return window;
}

struct gfx_window *gfx_screen_info() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    u32 width;
    u32 height;
    fb_size(&width, &height);
    CLEAR_CHECKPOINT();
    return gfx_new_window(width-1, height, 0, 0);
}

void gfx_draw_glyph(struct gfx_window *window, u16 x, u16 y, u16 w, u16 h, u16 pitch, u8 *data, u32 color) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    for (u16 i = 0; i < h; i++) {
        for (u16 j = 0; j < w; j++) {
            if (data[i * pitch + j / 8] & (1 << (j % 8))) {
                gfx_draw_pixel(window, x + j, y + i, color);
            }
        }
    }
    CLEAR_CHECKPOINT();

}

void setup_gfx() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    //fb_init();
    CLEAR_CHECKPOINT();
}