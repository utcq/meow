#include <libm/types.h>
#include <libm/assert.h>
#include <drivers/ps2/keyboard/kb.h>
#include <cpu/apic/ioapic.h>
#include <cpu/idt/idt.h>
#include <logger/log.h>

#include <checkpoint/checkpoint.h>


void handle_ps2_kb(struct isr_frame *) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    log_info("Handling PS2 Keyboard\n");
    CLEAR_CHECKPOINT();
}

void setup_ps2_keyboard() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    //log_running("Setting up PS2 Keyboard\n");
    //register_irq_entry(0x21, handle_ps2_kb);
    //ioapic_set_entry(0x01, 0x21, 0);
    //log_success("PS2 Keyboard setup\n");
    CLEAR_CHECKPOINT();
}