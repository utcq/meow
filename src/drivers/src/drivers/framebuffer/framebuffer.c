#include <libm/types.h>
#include <limapi/api.h>
#include <mem/pages/paging.h>
#include <libm/string.h>
#include <logger/log.h>

#include <checkpoint/checkpoint.h>

extern struct limine_framebuffer_request fb_req;
struct limine_framebuffer *fb = NULL;

u32 abs(s32 x) {
    return x < 0 ? -x : x;
}

void fb_size(u32 *width, u32 *height) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    *width = fb->width;
    *height = fb->height;
    CLEAR_CHECKPOINT();
}

void fb_pixel(u32 x, u32 y, u32 color) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    if (x >= fb->width || y >= fb->height) {
        CLEAR_CHECKPOINT();
        return;
    }
    u32 *fb_ptr = (u32 *)fb->address;
    fb_ptr[y * (fb->pitch / 4) + x] = color;
    CLEAR_CHECKPOINT();
}

void fb_clear(void) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    u32 width;
    u32 height;
    fb_size(&width, &height);
    for (u32 y = 0; y < height; y++) {
        for (u32 x = 0; x < width; x++) {
            fb_pixel(x, y, 0);
        }
    }
    CLEAR_CHECKPOINT();
}

void fb_line(u32 x0, u32 y0, u32 x1, u32 y1, u32 color) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    s32 dx = x1 - x0;
    s32 dy = y1 - y0;
    s32 steps = abs(dx) > abs(dy) ? abs(dx) : abs(dy);
    s32 x_inc = (s32)dx / (s32)steps;
    s32 y_inc = (s32)dy / (s32)steps;
    s32 x = x0;
    s32 y = y0;
    for (s32 i = 0; i <= steps; i++) {
        fb_pixel((u32)x, (u32)y, color);
        x += x_inc;
        y += y_inc;
    }
    CLEAR_CHECKPOINT();
}

void fb_rect(u32 x, u32 y, u32 w, u32 h, u32 color) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    fb_line(x, y, x + w, y, color);
    fb_line(x, y, x, y + h, color);
    fb_line(x + w, y, x + w, y + h, color);
    fb_line(x, y + h, x + w, y + h, color);
    CLEAR_CHECKPOINT();
}

void fb_fill_rect(u32 x, u32 y, u32 w, u32 h, u32 color) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    for (u32 i = 0; i < h; i++) {
        fb_line(x, y + i, x + w, y + i, color);
    }
    CLEAR_CHECKPOINT();
}

void fb_init(void) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    fb = fb_req.response->framebuffers[0];
    map_pages((u64)fb->address, (u64)VIRTUAL_TO_PHYSICAL(fb->address), fb->width * fb->height * 4, PAGE_PRESENT | PAGE_WRITE);
    memset((void *)fb->address, 0, fb->width * fb->height * 4);
    fb_clear();
    CLEAR_CHECKPOINT();
}
