#include <libm/types.h>

#ifndef DRIVER_FB_H
#define DRIVER_FB_H

void fb_init();
void fb_clear();
void fb_pixel(u32 x, u32 y, u32 color);
void fb_line(u32 x0, u32 y0, u32 x1, u32 y1, u32 color);
void fb_rect(u32 x, u32 y, u32 w, u32 h, u32 color);
void fb_fill_rect(u32 x, u32 y, u32 w, u32 h, u32 color);
void fb_size(u32 *width, u32 *height);

#endif