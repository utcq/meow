PREFIX := aarch64

LINKERSCRIPT := src/arch/$(PREFIX)/linker.ld

override CC := aarch64-linux-gnu-gcc
override LD := aarch64-linux-gnu-ld

KCFLAGS += -Wall \
	-Wextra \
	-std=gnu11 \
	-ffreestanding \
	-fno-stack-protector \
	-fno-stack-check \
	-fno-lto \
	-DARCH_AARCH64

KDLDFLAGS += -m aarch64elf \
	-nostdlib \
	-pie \
	-z text \
	-z max-page-size=0x1000 \
	-T $(LINKERSCRIPT)

ALL_CFILES := $(shell find src/ -type f -name '*.c')
ALL_ASFILES := $(shell find src/ -type f -name '*.S')
CFILES := $(shell mkarch/path_helper.sh "$(ALL_CFILES)" "$(PREFIX)")
ASFILES := $(shell mkarch/path_helper.sh "$(ALL_ASFILES)" "$(PREFIX)")
