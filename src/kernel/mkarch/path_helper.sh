#!/bin/bash
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <long_string_of_paths> <prefix>"
    exit 1
fi
LONG_STRING=$1
PREFIX=$2
IFS=' ' read -r -a PATHS <<< "$LONG_STRING"
FILTERED_PATHS=()
for PATH in "${PATHS[@]}"; do
    if [[ "$PATH" != *"arch/"* ]]; then
        # If the path does not contain "arch/", include it
        FILTERED_PATHS+=("$PATH")
    elif [[ "$PATH" == *"arch/$PREFIX"* ]]; then
        # If the path contains "arch/" and the prefix, include it
        FILTERED_PATHS+=("$PATH")
    fi
done
echo "${FILTERED_PATHS[@]}"
