PREFIX := riscv64

LINKERSCRIPT := src/arch/$(PREFIX)/linker.ld

override CC := riscv64-elf-gcc
override LD := riscv64-elf-ld

KCFLAGS += -Wall \
	-Wextra \
	-std=gnu11 \
	-ffreestanding \
	-fno-stack-protector \
	-fno-stack-check \
	-fno-lto \

KDLDFLAGS += -m elf64lriscv \
	-nostdlib \
	-z max-page-size=0x1000 \
	-T $(LINKERSCRIPT)

ALL_CFILES := $(shell find src/ -type f -name '*.c')
ALL_ASFILES := $(shell find src/ -type f -name '*.S')
CFILES := $(shell mkarch/path_helper.sh "$(ALL_CFILES)" "$(PREFIX)")
ASFILES := $(shell mkarch/path_helper.sh "$(ALL_ASFILES)" "$(PREFIX)")
