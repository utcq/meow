#ifndef LOGGER_LOG_H
#define LOGGER_LOG_H

void log_print(const char *message);
void log_printf(const char *format, ...);
void log_info(const char *format, ...);
void log_critical(const char *format, ...);
void log_failed_assertion(const char *format, ...);
void log_success(const char *format, ...);
void log_running(const char *format, ...);
void log_debug(const char *format, ...);

#endif