#include <stdarg.h>
#include <arch/serial.h>
#include <logger/log.h>
#include <libm/string.h>

void print_serial(const char *message) {
    /*
    @details: Print a string to the serial port.
    */
    while (*message) {
        write_serial(*message);
        message++;
    }
}

void log_print(const char *message) {
    /*
    @details: Log a message.
    */
    print_serial(message);
    // TODO: Print to the screen
}

void log_printf(const char *format, ...) {
    /*
    @details: Log a formatted message.
    */
    va_list args;
    va_start(args, format);
    char buffer[1024];
    vsprintf(buffer, format, args);
    log_print(buffer);
    va_end(args);
}

void log_vsprintf(const char *format, va_list args) {
    /*
    @details: Log a formatted message with a va_list.
    */
    char buffer[1024];
    vsprintf(buffer, format, args);
    log_print(buffer);
}

void log_info(const char *format, ...) {
    /*
    @details: Log an info message.
    */
    char base[128] = "\x1b[37;1m[INFO]\x1b[36m ";
    strcat(base, format);
    va_list args;
    va_start(args, format);
    log_vsprintf(base, args);
    va_end(args);
    log_print("\x1b[0m");
}

void log_critical(const char *format, ...) {
    /*
    @details: Log a critical message.
    */
    char base[128] = "\x1b[31;1m[CRITICAL]\x1b[36m ";
    strcat(base, format);
    va_list args;
    va_start(args, format);
    log_vsprintf(base, args);
    va_end(args);
    log_print("\x1b[0m");
}

void log_failed_assertion(const char *format, ...) {
    /*
    @details: Log a failed assertion message.
    */
    char base[128] = "\x1b[31;1m[CRITICAL]\x1b[36m \x1b[91m[ASSERTION]: \x1b[0m";
    strcat(base, format);
    va_list args;
    va_start(args, format);
    log_vsprintf(base, args);
    va_end(args);
    log_print("\n\x1b[0m");
}

void log_success(const char *format, ...) {
    /*
    @details: Log a success message.
    */
    char base[128] = "\x1b[32;1m[SUCCESS]\x1b[36m ";
    strcat(base, format);
    va_list args;
    va_start(args, format);
    log_vsprintf(base, args);
    va_end(args);
    log_print("\x1b[0m");
}

void log_running(const char *format, ...) {
    /*
    @details: Log a running message.
    */
    char base[128] = "\x1b[90;1m[RUNNING]\x1b[36m ";
    strcat(base, format);
    va_list args;
    va_start(args, format);
    log_vsprintf(base, args);
    va_end(args);
    log_print("\x1b[0m");
}

void log_debug(const char *format, ...) {
    /*
    @details: Log a debug message.
    */
    char base[128] = "\x1b[34;1m[DEBUG]\x1b[36m ";
    strcat(base, format);
    va_list args;
    va_start(args, format);
    log_vsprintf(base, args);
    va_end(args);
    log_print("\x1b[0m");
}