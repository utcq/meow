#include <limapi/api.h>
#include <libm/types.h>
#include <libm/assert.h>
#include <libm/string.h>
#include <logger/log.h>

#ifdef __x86_64__
#define LIMINE_PAGING_MODE_DEFAULT LIMINE_PAGING_MODE_X86_64_4LVL
#endif

LIMINE_BASE_REVISION(BASE_REVISION);

struct limine_framebuffer_request fb_req = {
    .id = LIMINE_FRAMEBUFFER_REQUEST,
    .revision = BASE_REVISION
};

struct limine_memmap_request memmap = {
    .id = LIMINE_MEMMAP_REQUEST,
    .revision = BASE_REVISION
};

static volatile struct limine_hhdm_request hhdm = {
    .id = LIMINE_HHDM_REQUEST,
    .revision = BASE_REVISION
};

static volatile struct limine_paging_mode_request paging_req = {
    .id = LIMINE_PAGING_MODE_REQUEST,
    .revision = BASE_REVISION,
    .mode = LIMINE_PAGING_MODE_DEFAULT,
    .flags = 0
};

static volatile struct limine_module_request module_req = {
    .id = LIMINE_MODULE_REQUEST,
    .revision = BASE_REVISION
};

struct limine_kernel_address_request kernaddress_req = {
    .id = LIMINE_KERNEL_ADDRESS_REQUEST,
    .revision = BASE_REVISION
};

static volatile struct limine_rsdp_request rsdp_req = {
    .id = LIMINE_RSDP_REQUEST,
    .revision = BASE_REVISION,
};

void *get_framebuffer(void) {
    assert(fb_req.response->framebuffer_count > 0, "No framebuffers available\n");
    log_success("Framebuffers available\n");
    return NULL;
}

struct limine_memmap_response *get_memmap(void) {
    return memmap.response;
}

struct limine_file *initrd_file;

void *get_hhdm_mem() {
    return (void*)hhdm.response->offset;
}

struct limine_memmap_entry *get_biggest_memory(void) {
    struct limine_memmap_entry *biggest = NULL;
    for (size_t i = 0; i < memmap.response->entry_count; i++) {
        struct limine_memmap_entry *entry = memmap.response->entries[i];
        if (entry->type != LIMINE_MEMMAP_USABLE)
            continue;
        if (!biggest || entry->length > biggest->length)
            biggest = entry;
    }
    return biggest;
}

struct limine_memmap_entry *get_2nd_biggest_memory(void) {
    struct limine_memmap_entry *biggest = get_biggest_memory();
    struct limine_memmap_entry *second_biggest = NULL;
    for (size_t i = 0; i < memmap.response->entry_count; i++) {
        struct limine_memmap_entry *entry = memmap.response->entries[i];
        if (entry->type != LIMINE_MEMMAP_USABLE)
            continue;
        if (entry == biggest)
            continue;
        if (!second_biggest || entry->length > second_biggest->length)
            second_biggest = entry;
    }
    return second_biggest;
}

void *get_rsdp_address(void) {
     return rsdp_req.response->address;   
}

void setup_modules(void) {
    for (size_t i = 0; i < module_req.response->module_count; i++) {
        struct limine_file *entry = module_req.response->modules[i];
        if (strcmp(entry->path, "/deps/initrd.cpio.gz") == 0) {
            initrd_file = entry;
        }
    }
    assert(initrd_file != NULL, "Missing initrd\n");
}

void setup_limine_api(void) {
    get_framebuffer();
    setup_modules();
    log_success("Limine API setup\n");
}
