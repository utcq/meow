#include <libm/types.h>

#ifndef LIMAPI_API_H
#define LIMAPI_API_H
#include <limine.h>

#define BASE_REVISION 2

void *get_framebuffer();
struct limine_memmap_response *get_memmap();
struct limine_memmap_entry *get_biggest_memory();
struct limine_memmap_entry *get_2nd_biggest_memory();
void *get_rsdp_address();
void *get_hhdm_mem();
void setup_limine_api();

#endif