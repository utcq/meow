#include <libm/types.h>
#include <cpu/pit/pit.h>
#include <arch/serial.h>
#include <cpu/pic/pic.h>
#include <cpu/apic/lapic.h>
#include <cpu/idt/idt.h>

#include <logger/log.h>
#include <checkpoint/checkpoint.h>

u64 uptime_ss = 0;
u16 uptime_ms = 0;
u64 countdown = 0;

void pit_set_divisor(u16 divisor) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    if (divisor < 100)
        divisor = 100;
    outb(PIT_DATA, (u8)(divisor & 0x00ff));
    io_wait();
    outb(PIT_DATA, (u8)((divisor & 0xff00) >> 8));
    CLEAR_CHECKPOINT();
}

u16 pit_read_counter() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    u16 count = 0;
    __asm__ volatile("cli");
    outb(PIT_COMMAND, 0x00);

    count = inb(PIT_DATA);
    count |= inb(PIT_DATA) << 8;

    __asm__ volatile("sti");

    CLEAR_CHECKPOINT();
    return count;
}

void pit_set_counter(u16 count) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    __asm__ volatile("cli");

    outb(PIT_DATA, count & 0xFF);
    outb(PIT_DATA, (count & 0xFF00) >> 8);

    __asm__ volatile("sti");
    CLEAR_CHECKPOINT();
}

void setup_pit() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    pit_set_counter(0);
    pit_set_divisor(FREQUENCY / FREQ_DIV);
    countdown=0;
    uptime_ms=0;
    uptime_ss=0;
    CLEAR_CHECKPOINT();
}

u64 pit_get_uptime_ms() {
    return uptime_ms;
}

u64 pit_get_uptime_ss() {
    return uptime_ss;
}

void pit_sleep(u64 millis) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    countdown = millis;
    
    while(countdown > 0) {
        __asm__ volatile("hlt");
    }
    CLEAR_CHECKPOINT();
}

void pit_handler(struct irq_frame *frame) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);

    uptime_ms++;
    if(uptime_ms % 1000 == 0) {
        uptime_ss++;
    }
    if(countdown > 0) {
        countdown--;
    }


    pic_send_eoi(0);
    lapic_ack_irq();

    CLEAR_CHECKPOINT();
}