#include <libm/types.h>
#include <cpu/idt/idt.h>

#ifndef CPU_PIT_H
#define CPU_PIT_H

#define PIT_COMMAND 0x43
#define PIT_DATA    0x40

#define FREQUENCY 1193182
#define FREQ_DIV  1000

void pit_set_counter(u16 counter);
u16 pit_get_counter();
void pit_set_divisor(u16 divisor);
void setup_pit();
void pit_handler(struct irq_frame *frame); 
void pit_sleep(u64 ms);

u64 pit_get_uptime_ms();
u64 pit_get_uptime_ss();

#endif