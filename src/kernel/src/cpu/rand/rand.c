#include <cpu/rand/rand.h>
#include <libm/types.h>
#include <arch/cmos/cmos.h>
#include <logger/log.h>
#include <arch/arch.h>

#include <checkpoint/checkpoint.h>

static u64 next = 1;

u64 rand(void) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    /*
    @details: C-Standard random number generation
    @RAND_MAX: 90112
    */
    next = next * 0x41C64E6D + 0x3039;
    CLEAR_CHECKPOINT();
    return (unsigned int) (next / 0x10000) % 0x16000;
}

u64 rrand(u64 min, u64 max) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    /*
    @details: Generate a random number in a range
    */
    CLEAR_CHECKPOINT();
    return rand() % (max + 1 - min) + min;
}

void srand(u64 seed) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    /*
    @details: Set seed for random number generation
    */
    next = seed;
    CLEAR_CHECKPOINT();
}

void setup_RNG(void) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    /*
    @details: Generate random seed from Time Stamp Counter
    */
    srand(rand_seed());
    log_success("RNG Setup [RDTSC]\n");
    CLEAR_CHECKPOINT();
}
