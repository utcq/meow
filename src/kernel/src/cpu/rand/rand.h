#include <libm/types.h>

#ifndef CPU_RAND_H
#define CPU_RAND_H

u64 rand();
u64 rrand(u64 min, u64 max);
void srand(u64 seed);
void setup_RNG();

#endif