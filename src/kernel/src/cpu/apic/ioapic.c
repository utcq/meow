#include <libm/types.h>
#include <cpu/apic/ioapic.h>
#include <logger/log.h>
#include <acpi/acpi.h>
#include <mem/pages/paging.h>

#include <checkpoint/checkpoint.h>

void *ioapic_address;

void mmio_write32(void *p, u32 data) {
    *(volatile u32 *)(p) = data;
}

static inline u32 mmio_read32(void *p) {
    return *(volatile u32 *)(p);
}

static void ioapic_out(u8 reg, u32 val) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    mmio_write32(ioapic_address + IOREGSEL, reg);
    mmio_write32(ioapic_address + IOWIN, val);
    CLEAR_CHECKPOINT();
}

static u32 ioapic_in(u8 reg) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    mmio_write32(ioapic_address + IOREGSEL, reg);
    return mmio_read32(ioapic_address + IOWIN);
    CLEAR_CHECKPOINT();
}

void ioapic_set_entry(u8 index, u8 irq, u32 cpuid) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    ioapic_out(IOREDTBL+2*index, (u32)irq);
    ioapic_out(IOREDTBL+2*index+1, cpuid << 24);
    CLEAR_CHECKPOINT();
}


void setup_ioapic() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    struct MADT_IOAPIC *ioapic = acpi_madt_get_ioapic();
    ioapic_address = PHYSICAL_TO_VIRTUAL(ioapic->ioapic_address);
    map_page((u64)ioapic_address, ioapic->ioapic_address, PAGE_PRESENT | PAGE_WRITE | PAGE_NX | PAGE_UNCACHEABLE);
    log_info("IOAPIC: %p\n", ioapic->ioapic_address);
    log_info("GSIB: %p\n", ioapic->global_system_interrupt_base);
    u32 entries = ioapic_in(IOAPICVER);
    u32 count = ((entries >> 16) & 0xFF); // maximum redirection entry
    for (u16 i = 0; i < count; ++i) { // Disable entries
        ioapic_set_entry(i, (0x00010000 | (0x20+i)), 0);
    }
    CLEAR_CHECKPOINT();
}
