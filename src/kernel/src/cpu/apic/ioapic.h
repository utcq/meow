#include <libm/types.h>

#ifndef CPU_APIC_IOAPIC_H
#define CPU_APIC_IOAPIC_H

#define IOAPICID          0x00
#define IOAPICVER         0x01
#define IOAPICARB         0x02
#define IOREDTBL          0x10
#define IOREGSEL          0x00
#define IOWIN             0x10


void setup_ioapic();
void ioapic_set_entry(u8 index, u8 irq, u32 cpuid);

#endif
