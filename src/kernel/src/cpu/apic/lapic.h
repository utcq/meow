#include <libm/types.h>

#ifndef CPU_LAPIC_H
#define CPU_LAPIC_H

#define PIC_COMMAND_MASTER 0x20
#define PIC_DATA_MASTER 0x21

#define PIC_COMMAND_SLAVE 0xA0
#define PIC_DATA_SLAVE 0xA1

#define ICW_1 0x11

#define ICW_2_M 0x20
#define ICW_2_S 0x28

#define ICW_3_M 0x02
#define ICW_3_S 0x04

#define ICW_4 0x01

#define IA_32_APIC_BASE 0x1B

#define LAPIC_SIV_REG 0xF0
#define LAPIC_EOI_REG 0xB0
#define LAPIC_ID_REG 0x20

void lapic_ack_irq();

void setup_lapic();

#endif