#include <libm/types.h>
#include <libm/string.h>
#include <cpu/apic/lapic.h>
#include <arch/serial.h>
#include <arch/arch.h>
#include <logger/log.h>
#include <mem/pages/paging.h>
#include <mem/heap/heap.h>
#include <cpu/apic/ioapic.h>
#include <acpi/acpi.h>
#include <libm/assert.h>
#include <checkpoint/checkpoint.h>

// Local APIC registers, divided by 4 for use as uint[] indices.
#define ID      (0x0020/4)   // ID
#define VER     (0x0030/4)   // Version
#define TPR     (0x0080/4)   // Task Priority
#define EOI     (0x00B0/4)   // EOI
#define SVR     (0x00F0/4)   // Spurious Interrupt Vector
  #define ENABLE     0x00000100   // Unit Enable
#define ESR     (0x0280/4)   // Error Status
#define ICRLO   (0x0300/4)   // Interrupt Command
  #define INIT       0x00000500   // INIT/RESET
  #define STARTUP    0x00000600   // Startup IPI
  #define DELIVS     0x00001000   // Delivery status
  #define ASSERT     0x00004000   // Assert interrupt (vs deassert)
  #define DEASSERT   0x00000000
  #define LEVEL      0x00008000   // Level triggered
  #define BCAST      0x00080000   // Send to all APICs, including self.
  #define BUSY       0x00001000
  #define FIXED      0x00000000
#define ICRHI   (0x0310/4)   // Interrupt Command [63:32]
#define TIMER   (0x0320/4)   // Local Vector Table 0 (TIMER)
  #define X1         0x0000000B   // divide counts by 1
  #define PERIODIC   0x00020000   // Periodic
#define PCINT   (0x0340/4)   // Performance Counter LVT
#define LINT0   (0x0350/4)   // Local Vector Table 1 (LINT0)
#define LINT1   (0x0360/4)   // Local Vector Table 2 (LINT1)
#define ERROR   (0x0370/4)   // Local Vector Table 3 (ERROR)
  #define MASKED     0x00010000   // Interrupt masked
#define TICR    (0x0380/4)   // Timer Initial Count
#define TCCR    (0x0390/4)   // Timer Current Count
#define TDCR    (0x03E0/4)   // Timer Divide Configuration
#define T_IRQ0  0x20
#define IRQ_TIMER 0
#define IRQ_SPURIOUS 0x21
#define IRQ_ERROR 0x13
#define FREQ  10000000

/*void disable_pic() {
    outb(PIC_COMMAND_MASTER, ICW_1);
    outb(PIC_COMMAND_SLAVE, ICW_1);
    outb(PIC_DATA_MASTER, ICW_2_M);
    outb(PIC_DATA_SLAVE, ICW_2_S);
    outb(PIC_DATA_MASTER, ICW_3_M);
    outb(PIC_DATA_SLAVE, ICW_3_S);
    outb(PIC_DATA_MASTER, ICW_4);
    outb(PIC_DATA_SLAVE, ICW_4);
    outb(PIC_DATA_MASTER, 0xFF);
    outb(PIC_DATA_SLAVE, 0xFF);
    outb(PIC_COMMAND_MASTER+1, 0xFF);
    outb(PIC_COMMAND_SLAVE+1, 0xFF);
}

void lapic_write(u32 reg, u32 data) {
    *(volatile u32*)(lapic_base_virt+reg)=data;
}

u32 lapic_read(u32 reg) {
    return *(volatile u32*)(lapic_base_virt+reg);
}

void lapic_enable() {
    // Single core LAPIC
    u64 lapic_r = rdmsr(IA_32_APIC_BASE);
    u64 lapic_addr = lapic_r & 0xFFFFFFFFFFFFF000;
    lapic_base_virt = (u64)PHYSICAL_TO_VIRTUAL(lapic_addr);
    log_info("LAPIC Address: %x\n", lapic_addr);
    map_page(lapic_base_virt, lapic_addr, PAGE_PRESENT | PAGE_UNCACHEABLE | PAGE_WRITE | PAGE_NX);
    wrmsr(IA_32_APIC_BASE, lapic_r | (1 << 11)); // Enable APIC if isn't already
    u16 siv_value = lapic_read(LAPIC_SIV_REG) | (1 << 8); // Enable spurious interrupt vector
    siv_value &= ~(u8)(0xFF); // Clear spurious interrupt vector
    siv_value |= (u8)(0xF0); // Set spurious interrupt vector to 0xF0
    lapic_write(LAPIC_SIV_REG, siv_value);
}

void timer_init() {
    lapic_write(TDCR, X1);
    lapic_write(TIMER, PERIODIC | (T_IRQ + IRQ_TIMER));
    lapic_write(TICR, FREQ);
}

void lapic_cfg() {
    lapic_write(LINT0, MASKED);
    lapic_write(LINT1, MASKED);

    lapic_write(ESR, 0);
    lapic_write(ESR, 0);

    lapic_write(EOI, 0);
    lapic_write(TPR, 0);
    lapic_write(IHCR, 0);
}

void setup_lapic() {
    disable_pic();
    lapic_enable();
    timer_init();
    lapic_cfg();
    setup_ioapic();
}*/

static u32 *lapic;

void disable_pic() {
  outb(PIC_COMMAND_MASTER, ICW_1);
  outb(PIC_COMMAND_SLAVE, ICW_1);
  outb(PIC_DATA_MASTER, ICW_2_M);
  outb(PIC_DATA_SLAVE, ICW_2_S);
  outb(PIC_DATA_MASTER, ICW_3_M);
  outb(PIC_DATA_SLAVE, ICW_3_S);
  outb(PIC_DATA_MASTER, ICW_4);
  outb(PIC_DATA_SLAVE, ICW_4);
  outb(PIC_DATA_MASTER, 0xFF);
  outb(PIC_DATA_SLAVE, 0xFF);
  outb(PIC_COMMAND_MASTER+1, 0xFF);
  outb(PIC_COMMAND_SLAVE+1, 0xFF);
}

static void lapicw(int index, int value) {
  lapic[index] = value;
  lapic[ID];  // wait for write to finish, by reading
}

void lapic_ack_irq(void) {
  if(lapic)
    lapicw(EOI, 0);
}

void setup_lapic(void) {
  CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);

  disable_pic();

  lapic = (u32*)get_acpi_lapic_address();
  map_page((u64)lapic, (u64)lapic, PAGE_PRESENT | PAGE_WRITE | PAGE_NX | PAGE_UNCACHEABLE);
  log_debug("LAPIC Address: %p\n", get_acpi_lapic_address());
  assert(lapic != NULL, "LAPIC not found\n");

  lapicw(SVR, ENABLE | (T_IRQ0 + IRQ_SPURIOUS));

  lapicw(TDCR, X1);
  lapicw(TIMER, PERIODIC | (T_IRQ0 + IRQ_TIMER));
  lapicw(TICR, FREQ);

  lapicw(LINT0, MASKED);
  lapicw(LINT1, MASKED);

  if(((lapic[VER]>>16) & 0xFF) >= 4)
      lapicw(PCINT, MASKED);

  lapicw(ERROR, T_IRQ0 + IRQ_ERROR);
  lapicw(ESR, 0);
  lapicw(ESR, 0);
  lapicw(EOI, 0);
  lapicw(ICRHI, 0);
  lapicw(ICRLO, BCAST | INIT | LEVEL);
  while(lapic[ICRLO] & DELIVS);
  lapicw(TPR, 0);

  CLEAR_CHECKPOINT();
}