#include <libm/types.h>
#include <cpu/syscalls/sysc.h>
#include <cpu/idt/idt.h>
#include <logger/log.h>
#include <libm/assert.h>

#include <checkpoint/checkpoint.h>

#define SYSCALL_LIMIT 32

u64 sys_testsyscalls(void) {
    log_info("Testing syscalls\n");
    return 0x10;
}


/*void *syscall_handlers[SYSCALL_LIMITS] = {
    SYSCALLS_LIST
};*/

typedef u64 (*syscall_t)(u64, u64, u64, u64, u64, u64);

#define PARAM(type, name, fmt) type name
#define SYSCALL(name, n_args, ret_type, ...) \
  ret_type weak sys_ ##name(MACRO_JOIN(__VA_ARGS__)) { \
    assert(0==1, "syscall not implemented: " #name " (%d)", SYS_ ##name); \
  };


#define PARAM(type, name, fmt)
#define SYSCALL(name, n_args, ret_type, ...) (void *) sys_ ##name,
static syscall_t syscall_handlers[SYSCALL_LIMIT] = {
#include <cpu/syscalls/syscalls_h.def.h>
};

u64 syscall_handler(struct syscall_frame *frame) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    log_info("Syscall number: %d\n", frame->number);
    u32 syscall_num = frame->number;
    assert(syscall_num < SYSCALL_LIMIT, "Syscall number out of bounds");
    syscall_t fn = syscall_handlers[syscall_num];
    assert(fn != 0, "Syscall handler not found");
    u64 ret = fn(frame->arg1, frame->arg2, frame->arg3, frame->arg4, frame->arg5, frame->arg6);
    CLEAR_CHECKPOINT();
    return ret;
}

void syscalls_init() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    create_idt_entry(0x80, (u64)isr0x80, 0x08, 0x8E);
    CLEAR_CHECKPOINT();
}