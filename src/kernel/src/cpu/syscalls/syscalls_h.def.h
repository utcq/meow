#ifndef SYSCALL
#define SYSCALL(name, n_args, ret_type, ...)
#endif
#ifndef PARAM
#define PARAM(type, name, fmt)
#endif


#ifndef SYS_MAX
#define SYS_MAX SYS_execveat
#endif

SYSCALL(testsyscalls, 3, void)

#undef PARAM
#undef SYSCALL