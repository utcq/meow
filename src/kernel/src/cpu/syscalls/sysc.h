#include <libm/types.h>


#ifndef CPU_SYSCALLS_H
#define CPU_SYSCALLS_H

struct syscall_frame {
    u64 number;
    u64 arg1;
    u64 arg2;
    u64 arg3;
    u64 arg4;
    u64 arg5;
    u64 arg6;
};

void syscalls_init();

#endif