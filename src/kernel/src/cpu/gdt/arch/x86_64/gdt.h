#include <libm/types.h>

#ifndef CPU_X86_64_GDT_H
#define CPU_X86_64_GDT_H

struct gdt_entry {
    u16 limit_low;
    u16 base_low;
    u8  base_middle;
    u8  access;
    u8  granularity;
    u8  base_high;
} __attribute__((packed));

typedef struct gdt_ptr {
    u16 limit;
    u64 base;
} __attribute__ ((packed)) gdt_ptr_t;

void setup_gdt();

#endif
