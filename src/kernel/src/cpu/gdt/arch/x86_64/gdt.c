#include <libm/types.h>
#include <cpu/gdt/gdt.h>
#include <logger/log.h>
#include <mem/heap/heap.h>
#include <mem/pages/paging.h>

#include <checkpoint/checkpoint.h>

#define ENTRIES 5

extern void load_gdt(void* gdt_addr);
extern void reload_cs_segments();
gdt_ptr_t gp;
struct gdt_entry table[ENTRIES];

u8 entry_i=0;
void set_gdt_entry(int index, u32 base, u32 limit, u8 access, u8 granularity) {
  CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
  /*
  @details: Standard implementation of idt entry setup
  */
  table[index].base_low = (base & 0xFFFF);
  table[index].base_middle = (base >> 16) & 0xFF;
  table[index].base_high = (base >> 24) & 0xFF;

  table[index].limit_low = (limit & 0xFFFF);
  table[index].granularity = (limit >> 16) & 0x0F;

  table[index].granularity |= (granularity & 0xF0);
  table[index].access = access;
  CLEAR_CHECKPOINT();
}
void setup_gdt() {
  CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
  /*
  @details: Setup GDT entries and load GDT+reload segments
            Standard GDT setup
  */
	gp.limit = (sizeof(struct gdt_entry) * ENTRIES) - 1;
	gp.base = (u64)&table;

  set_gdt_entry(0, 0, 0, 0, 0); // Null Seg
  set_gdt_entry(1, 0, 0xFFFFF, 0x9A, 0xA0); // Kernel Code Seg
  set_gdt_entry(2, 0, 0xFFFFF, 0x92, 0xA0); // Kernel Data Seg
  set_gdt_entry(3, 0, 0xFFFFF, 0xFA, 0xC0); // User Code Seg
  set_gdt_entry(4, 0, 0xFFFFF, 0xF2, 0xC0); // User Data Seg

  // Future TSS entry here (Task State Segment)

	load_gdt(&gp);
  log_success("GDT loaded\n");
  reload_cs_segments();
  log_success("Segments reloaded\n");
  CLEAR_CHECKPOINT();
}
