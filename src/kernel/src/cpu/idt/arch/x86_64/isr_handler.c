#include <libm/types.h>
#include <cpu/idt/arch/x86_64/idt.h>
#include <logger/log.h>
#include <checkpoint/checkpoint.h>
#include <libm/string.h>

extern struct checkpoint checkpoint_source;
extern struct checkpoint *checkpoint_list;

const char *dump_fmt = "\n\
* * * * *\n\
\x1b[32;1mINT: \x1b[36;3;4m%p\x1b[0m \x1b[32;1mERR: \x1b[36;3;4m%p\x1b[0m\n\
\x1b[32;1mRAX: \x1b[36;3;4m%p\x1b[0m \x1b[32;1mRBX: \x1b[36;3;4m%p\x1b[0m \x1b[32;1mRCX: \x1b[36;3;4m%p\x1b[0m \x1b[32;1mRDX: \x1b[36;3;4m%p\x1b[0m \x1b[32;1mRSI: \x1b[36;3;4m%p\x1b[0m\n\
\x1b[32;1mRDI: \x1b[36;3;4m%p\x1b[0m \x1b[32;1mRSP: \x1b[36;3;4m%p\x1b[0m \x1b[32;1mRBP: \x1b[36;3;4m%p\x1b[0m \x1b[32;1mR8: \x1b[36;3;4m%p\x1b[0m\n\
\x1b[32;1mR9: \x1b[36;3;4m%p\x1b[0m \x1b[32;1mR10: \x1b[36;3;4m%p\x1b[0m\n\
\x1b[32;1mR11: \x1b[36;3;4m%p\x1b[0m \x1b[32;1mR12: \x1b[36;3;4m%p\x1b[0m \x1b[32;1mR13: \x1b[36;3;4m%p\x1b[0m \x1b[32;1mR14: \x1b[36;3;4m%p\x1b[0m \x1b[32;1mR15: \x1b[36;3;4m%p\x1b[0m\n\
\x1b[32;1mRIP: \x1b[36;3;4m%p\x1b[0m\n\
\x1b[32;1mCR0: \x1b[36;3;4m%p\x1b[0m \x1b[32;1mCR2: \x1b[36;3;4m%p\x1b[0m \x1b[32;1mCR3: \x1b[36;3;4m%p\x1b[0m \x1b[32;1mCR4: \x1b[36;3;4m%p\x1b[0m \x1b[32;1mCR8: \x1b[36;3;4m%p\x1b[0m\n\
\x1b[32;1mGS: \x1b[36;3;4m%p\x1b[0m \x1b[32;1mFS: \x1b[36;3;4m%p\x1b[0m \x1b[32;1mES: \x1b[36;3;4m%p\x1b[0m \x1b[32;1mDS: \x1b[36;3;4m%p\x1b[0m \x1b[32;1mCS: \x1b[36;3;4m%p\x1b[0m \x1b[32;1mSS: \x1b[36;3;4m%p\x1b[0m\n\x1b[0m\
* * * * *\n\
\n";

void info_dump(struct isr_frame *info) {
    log_printf(dump_fmt, info->int_num, info->error_code, info->rax,
        info->rbx, info->rcx, info->rdx, info->rsi, info->rdi, info->rsp,
        info->rbp, info->r8, info->r9, info->r10, info->r11, info->r12,
        info->r13, info->r14, info->r15, info->rip, info->cr0, info->cr2,
        info->cr3, info->cr4, info->cr8, info->gs, info->fs, info->es,
        info->ds, info->cs, info->ss);
}

const char *messages[] = {
    "Divide by zero",
    "Single step",
    "Non-maskable interrupt",
    "Breakpoint",
    "Overflow",
    "Bound range exceeded",
    "Invalid opcode",
    "Device not available",
    "Double fault",
    "Coprocessor segment overrun",
    "Invalid TSS",
    "Segment not present",
    "Stack-segment fault",
    "General protection fault",
    "Page fault",
    "Reserved",
    "x87 FPU error",
    "Alignment check",
    "Machine check",
    "SIMD floating-point exception",
    "Virtualization exception",
    "Control protection exception",
    [0xF0] = "Spurious vector"
};

void handle_double_fault(struct isr_frame *info) {
    u8 ext = info->error_code & 0x01;
    u8 tbl = (info->error_code & 0x07) >> 1;
    u16 index = (info->error_code & 0xFFFF) >> 3;
    if (ext) {
        log_critical("Double fault: external\n");
    }
    if (tbl == 0b00) {
        log_critical("Double fault: GDT\n");
    } else if (tbl == 0b01 || tbl == 0b11) {
        log_critical("Double fault: IDT\n");
    } else {
        log_critical("Double fault: LDT\n");
    }
    log_critical("Double fault: index %d\n", index);
}

void handle_invalid_tss(struct isr_frame *info) {
    log_critical("TSS Index: %d\n", info->error_code);
}

void handle_invalid_segment(struct isr_frame *info) {
    log_critical("Segment Index: %d\n", info->error_code);
}

void handle_gpf(struct isr_frame *info) {
    log_critical("Segment Index: %p\n", info->error_code);
}

void handle_page_fault(struct isr_frame *info) {
    u8 attrs = info->error_code & 0x7F;
    if ((attrs >> 0) & 0x1) {
        log_critical("Page fault: page-protection violation\n");
    } else {
        log_critical("Page fault: non-present page\n");
    }
    if ((attrs >> 1) & 0x1) {
        log_critical("Page fault: write access\n");
    } else {
        log_critical("Page fault: read access\n");
    }
    if ((attrs >> 2) & 0x1) {
        log_critical("Page fault: user-mode access\n");
    } else {
        log_critical("Page fault: supervisor-mode access\n");
    }
    if ((attrs >> 3) & 0x1) {
        log_critical("Page fault: reserved bit violation\n");
    }
    if ((attrs >> 4) & 0x1) {
        log_critical("Page fault: instruction fetch\n");
    }
    if ((attrs >> 5) & 0x1) {
        log_critical("Page fault: protection key violation\n");
    }
    if ((attrs >> 6) & 0x1) {
        log_critical("Page fault: Shadow Stack access\n");
    }
}

void handle_err_code(struct isr_frame *info) {
    if (info->int_num == 0x08) {
        handle_double_fault(info);
    }
    else if (info->int_num == 0x0A) {
        handle_invalid_tss(info);
    }
    else if (info->int_num == 0x0B) {
        handle_invalid_segment(info);
    }
    else if (info->int_num == 0x0D) {
        handle_gpf(info);
    }
    else if (info->int_num == 0x0E) {
        handle_page_fault(info);
    }
}

void isr_handler(struct isr_frame *info) {
    log_critical("Exception in %s on %s()\n", checkpoint_source.source, checkpoint_source.function);
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    info_dump(info);
    if (info->int_num < 21) {
        log_critical("Exception: \x1b[33m%s\n", messages[info->int_num]);
    } else {
        log_critical("Interrupt: \x1b[33m%d\n", info->int_num);
    }

    if (info->int_num == 0x08 || info->int_num == 0x0A || info->int_num == 0x0B || info->int_num == 0x0D || info->int_num == 0x0E) {
        handle_err_code(info);
    }
    
    log_info("Exception raised on: %p\n", info->return_address);

    CLEAR_CHECKPOINT();
    __asm__ volatile ("cli; hlt"); // Completely hangs the computer
}

