#include <libm/types.h>

#ifndef CPU_X86_64_IDT_H
#define CPU_X86_64_IDT_H
#include <cpu/idt/arch/x86_64/isr.h>

#define INT_GATE 0x8E
#define TRAP_GATE 0x8F

#define IRQ_BASE 0x20

struct idt_descriptor {
    u16 size;
    u64 offset;
} __attribute__((packed));

struct gate_descriptor {
    u16 offset_low;
    u16 selector;
    u8 ist;
    u8 type_attr;
    u16 offset_mid;
    u32 offset_high;
    u32 zero;
} __attribute__((packed));

void register_irq_entry(u8 irq, void (*handler)(struct irq_frame*));
void create_idt_entry(u8 index, u64 offset, u16 selector, u8 type);

void setup_idt();

#endif