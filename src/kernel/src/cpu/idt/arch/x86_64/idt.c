#include <libm/types.h>
#include <cpu/idt/idt.h>
#include <cpu/idt/arch/x86_64/isr.h>
#include <logger/log.h>

#include <checkpoint/checkpoint.h>

#define IDT_MAX_DESCRIPTORS 256

__attribute__((aligned(0x10))) static struct gate_descriptor idt[IDT_MAX_DESCRIPTORS];
static struct idt_descriptor idtr;

static void *isr_handlers[256] = {
    [0] = isr0,
    [1] = isr1,
    [2] = isr2,
    [3] = isr3,
    [4] = isr4,
    [5] = isr5,
    [6] = isr6,
    [7] = isr7,
    [8] = isr8,
    [9] = isr9,
    [10] = isr10,
    [11] = isr11,
    [12] = isr12,
    [13] = isr13,
    [14] = isr14,
    [15] = isr15,
    [16] = isr16,
    [17] = isr17,
    [18] = isr18,
    [19] = isr19,
    [20] = isr20,
    [21] = isr21,
    [22] = isr22,
    [23] = isr23,
    [24] = isr24,
    [25] = isr25,
    [26] = isr26,
    [27] = isr27,
    [28] = isr28,
    [29] = isr29,
    [30] = isr30,
    [31] = isr31,
    [32] = isr32,
    [33] = isr33,
    [34] = isr34,
    [35] = isr35,
    [0xF0] = isr0xF0
};

void create_idt_entry(u8 index, u64 offset, u16 selector, u8 type) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    idt[index].offset_low = offset & 0xFFFF;
    idt[index].selector = selector;
    idt[index].ist = 0;
    idt[index].type_attr = type | 0x80;
    idt[index].offset_mid = (offset >> 16) & 0xFFFF;
    idt[index].offset_high = (offset >> 32) & 0xFFFFFFFF;
    idt[index].zero = 0;
    CLEAR_CHECKPOINT();
}

void setup_idt() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    idtr.offset = (u64)&idt[0];
    idtr.size = (u16)(sizeof(struct gate_descriptor) * IDT_MAX_DESCRIPTORS) - 1;
    for (u8 i = 0; i < 255; i++) {
        if (isr_handlers[i] == NULL)
            continue;
        create_idt_entry(i, (u64)isr_handlers[i], 0x08, INT_GATE);
    }
    __asm__ volatile ("lidt %0" : : "m"(idtr));
    __asm__ volatile ("sti");
    log_success("IDT loaded\n");
    CLEAR_CHECKPOINT();
}