#include <libm/types.h>
#include <cpu/idt/arch/x86_64/idt.h>

#include <logger/log.h>

#include <checkpoint/checkpoint.h>

#include <taskman/task.h>

void *irq_handlers[256] = {0};

void register_irq_entry(u8 irq, void (*handler)(struct irq_frame*)) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    irq_handlers[irq] = handler;
    //create_idt_entry(irq, (u64)handler, 0x08, INT_GATE);
    CLEAR_CHECKPOINT();
}

void irq_handler(struct irq_frame *frame) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    log_debug("RA: %p\n", frame->return_address);
    if (irq_handlers[frame->int_num]) {
        void (*handler)(struct irq_frame*) = irq_handlers[frame->int_num];
        handler(frame);
    }

    taskman_schedule(frame);
    
    CLEAR_CHECKPOINT();
}