#include <cpu/pic/pic.h>
#include <libm/types.h>
#include <cpu/pit/pit.h>
#include <cpu/idt/idt.h>

#include <checkpoint/checkpoint.h>

#define IRQ_TIMER 0

void setup_periodic() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    register_irq_entry(IRQ_TIMER, (void*)pit_handler);
    pic_unmask(IRQ_TIMER);
    CLEAR_CHECKPOINT();
}