#include <arch/serial.h>
#include <libm/types.h>
#include <cpu/pic/pic.h>

#include <checkpoint/checkpoint.h>

static u16 pic_mask_v = 0xFFFF;

void pic_set_mask(u16 new_mask) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    pic_mask_v = new_mask;
    outb(PIC1_DATA, pic_mask_v & 0xFF);
    io_wait();
    outb(PIC2_DATA, pic_mask_v >> 8);
    io_wait();
    CLEAR_CHECKPOINT();
}

u16 pic_get_mask() {
    return inb(PIC1_DATA) | (inb(PIC2_DATA) << 8);
}

void setup_pic() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    u8 a1,a2;
    u8 offset1 = PIC_RM_OFFSET;
    u8 offset2 = PIC_RM_OFFSET + 8;

	a1 = inb(PIC1_DATA);                        // save masks
	a2 = inb(PIC2_DATA);
	
	outb(PIC1_COMMAND, PIC_ICW1_INIT | PIC_ICW1_ICW4);  // starts the initialization sequence (in cascade mode)
	io_wait();
	outb(PIC2_COMMAND, PIC_ICW1_INIT | PIC_ICW1_ICW4);
	io_wait();
	outb(PIC1_DATA, offset1);                 // ICW2: Master PIC vector offset
	io_wait();
	outb(PIC2_DATA, offset2);                 // ICW2: Slave PIC vector offset
	io_wait();
	outb(PIC1_DATA, 4);                       // ICW3: tell Master PIC that there is a slave PIC at IRQ2 (0000 0100)
	io_wait();
	outb(PIC2_DATA, 2);                       // ICW3: tell Slave PIC its cascade identity (0000 0010)
	io_wait();
	
	outb(PIC1_DATA, PIC_ICW4_8086);               // ICW4: have the PICs use 8086 mode (and not 8080 mode)
	io_wait();
	outb(PIC2_DATA, PIC_ICW4_8086);
	io_wait();
	
	outb(PIC1_DATA, a1);   // restore saved masks.
	outb(PIC2_DATA, a2);
    CLEAR_CHECKPOINT();
}

void pic_send_eoi(u8 irq) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    if(irq >= 8) {
        outb(PIC2_COMMAND, PIC_EOI);
    }
    outb(PIC1_COMMAND, PIC_EOI);
    CLEAR_CHECKPOINT();
}

void pic_disable() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    pic_set_mask(0xFFFF);
    CLEAR_CHECKPOINT();
}

void pic_mask(u8 irq) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    pic_set_mask(pic_mask_v | (1 << irq));
    CLEAR_CHECKPOINT();
}

void pic_unmask(u8 irq) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    pic_set_mask(pic_mask_v & ~(1 << irq));
    CLEAR_CHECKPOINT();
}

u16 pic_read_irq_reg() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    outb(PIC1_COMMAND, PIC_READ_IRR);
    outb(PIC2_COMMAND, PIC_READ_IRR);
    io_wait();
    CLEAR_CHECKPOINT();
    return (u16)inb(PIC2_COMMAND) | ((u16)inb(PIC1_COMMAND) << 8);
}

u16 pic_read_isr_reg() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    outb(PIC1_COMMAND, PIC_READ_ISR);
    outb(PIC2_COMMAND, PIC_READ_ISR);
    io_wait();
    CLEAR_CHECKPOINT();
    return (u16)inb(PIC2_COMMAND) | ((u16)inb(PIC1_COMMAND) << 8);
}