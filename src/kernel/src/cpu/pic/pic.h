#include <libm/types.h>

#ifndef CPU_PIC_H
#define CPU_PIC_H

#define PIC1_COMMAND 0x20
#define PIC1_DATA    0x21
#define PIC2_COMMAND 0xA0
#define PIC2_DATA    0xA1

#define PIC_ICW1_ICW4 0x01
#define PIC_ICW1_SINGLE 0x02
#define PIC_ICW1_INTERVAL4 0x04
#define PIC_ICW1_LEVEL 0x08
#define PIC_ICW1_INIT 0x10

#define PIC_ICW4_8086 0x01
#define PIC_ICW4_AUTO 0x02
#define PIC_ICW4_BUF_SLAVE 0x08
#define PIC_ICW4_BUF_MASTER 0x0C
#define PIC_ICW4_SFNM 0x10

#define PIC_EOI 0x20
#define PIC_READ_IRR 0x0A
#define PIC_READ_ISR 0x0B

#define PIC_RM_OFFSET 0x20

void setup_pic();
void pic_disable();
void pic_mask(u8 irq);
void pic_unmask(u8 irq);
void pic_send_eoi(u8 irq);

u16 pic_read_irq_reg();
u16 pic_read_isr_reg();

#endif