#include <libm/types.h>

#ifndef DISK_RAMDISK_H
#define DISK_RAMDISK_H

#define DESC_FILE 1
#define DESC_DIR 2

void setup_ramdisk(void);

struct fs_entry_t {
    union {
        struct file_t *file;
        struct dir_t *dir;
    };
    u8 type;
};

struct file_t {
    char *name;
    u32 size;
    void *ptr;
};

struct dir_t {
    char *name;
    struct fs_entry_t entries[32];
    u8 e_index;
};

struct file_t *get_file(const char *path);

#endif

