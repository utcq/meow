#include <disk/ramdisk.h>
#include <libm/types.h>
#include <libm/string.h>
#include <limapi/api.h>
#include <disk/carchive.h>
#include <logger/log.h>
#include <mem/heap/heap.h>

#include <checkpoint/checkpoint.h>

extern struct limine_file *initrd_file;
struct dir_t root_dir;

struct file_t *get_file(const char *path) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);

    for (u8 i = 0; i < root_dir.e_index; i++) {
        if (strcmp(root_dir.entries[i].file->name, path) == 0) {
            CLEAR_CHECKPOINT();
            return root_dir.entries[i].file;
        }
    }
    CLEAR_CHECKPOINT();
    return NULL;
}

void setup_ramdisk(void) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    root_dir.name = "/";
    root_dir.e_index = 0;
    read_carc(initrd_file->address, &root_dir);
    log_success("Ramdisk initialized\n");
    CLEAR_CHECKPOINT();
}