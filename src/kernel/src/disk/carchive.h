#include <libm/types.h>
#include <disk/ramdisk.h>

#ifndef DISK_CPIO_H
#define DISK_CPIO_H

#define CPIO_FILE (unsigned char)'F'
#define CPIO_DIR  (unsigned char)'D'

struct __attribute__((packed, scalar_storage_order("big-endian"))) carc_ar_t {
    char  h_type;
    char  h_check;
    u32   h_name_size;
    u64   h_file_size;
};

void read_carc(char *ptr, struct dir_t *root_dir);

#endif