#include <libm/types.h>
#include <libm/string.h>
#include <disk/carchive.h>
#include <logger/log.h>
#include <mem/heap/heap.h>
#include <disk/ramdisk.h>

#include <checkpoint/checkpoint.h>

u64 offset = 0;

u8 count_tokens_str(char *str, char delim) {
    u8 count = 0;
    u32 len = strlen(str);
    for (u32 i = 0; i < len; i++) {
        if (str[i] == delim) {
            count++;
        }
    }
    return count;
}

struct dir_t *carc_find_parent(char *path, struct dir_t *root_dir) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);

    char *parent_path = (char *)kmalloc(strlen(path)+1);
    memcpy(parent_path, path, strlen(path));

    struct dir_t *parent_dir = root_dir;
    char *token = strtok(parent_path, "/");
    token = strtok(NULL, "/");
    u8 tokcount = count_tokens_str(path, '/');
    u8 tok = 0;
    while (token != NULL) {
        if (tok == tokcount-1) {
            break;
        }
        log_info("Token: %s\n", token);
        for (u8 i = 0; i < parent_dir->e_index; i++) {
            log_info("%d Entry: %p\n", i, parent_dir->entries[i].dir->name);
            /*if (strcmp(parent_dir->entries[i].dir->name, token) == 0) {
                parent_dir = parent_dir->entries[i].dir;
                break;
            }*/
        }
        token = strtok(NULL, "/");
        tok++;
    }

    CLEAR_CHECKPOINT();
    return parent_dir;
}

void carc_parse_dir(char *ptr, struct carc_ar_t *header, struct dir_t *root_dir) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);

    /*char *path = kmalloc(header->h_name_size+1);
    memcpy(path+1, ptr+offset, header->h_name_size);
    path[0]='r';
    offset+=header->h_name_size;
    struct dir_t *parent_dir = carc_find_parent(path+1, root_dir);
    parent_dir->entries[parent_dir->e_index].dir = kmalloc(sizeof(struct dir_t));
    if (strcmp(path+1, "/") == 0) {
        return;
    }
    char *name;
    u8 tokcount = count_tokens_str(path, '/');
    if (tokcount == 0) {
        name = kmalloc(strlen(path) + 1); // Allocate memory for the name variable
        strcpy(name, path); // Copy the value of path into name
    } else {
        char *token = strtok(path, "/");
        for (u8 i = 0; i < tokcount+1; i++) {
            name = kmalloc(strlen(token) + 1); // Allocate memory for the name variable
            strcpy(name, token); // Copy the value of token into name
            token = strtok(NULL, "/");
        }
    }
    parent_dir->entries[parent_dir->e_index].dir->name = name;
    parent_dir->entries[parent_dir->e_index].dir->e_index = 0;
    parent_dir->entries[parent_dir->e_index].type = DESC_DIR;
    log_info("%d Name: %p\n", parent_dir->e_index, parent_dir->entries[parent_dir->e_index].dir->name);
    parent_dir->e_index++;*/

    offset+=header->h_name_size;

    CLEAR_CHECKPOINT();
}

void carc_parse_file(char *ptr, struct carc_ar_t *header, struct dir_t *root_dir) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);

    /*char *path = kmalloc(header->h_name_size+1);
    memcpy(path+1, ptr+offset, header->h_name_size);
    path[0]='r';
    offset+=header->h_name_size;
    struct dir_t *parent_dir = carc_find_parent(path, root_dir);
    parent_dir->entries[parent_dir->e_index].file = kmalloc(sizeof(struct file_t));
    char *name;
    u8 tokcount = count_tokens_str(path, '/');
    if (tokcount == 0) {
        name = path;
    } else {
        char *token = strtok(path, "/");
        for (u8 i = 0; i < tokcount+1; i++) {
            name = token;
            token = strtok(NULL, "/");
        }
    }
    
    parent_dir->entries[parent_dir->e_index].file->name = name;
    parent_dir->entries[parent_dir->e_index].file->size = header->h_file_size;
    parent_dir->entries[parent_dir->e_index].file->ptr = ptr+offset;
    parent_dir->entries[parent_dir->e_index].type = DESC_FILE;
    parent_dir->e_index++;*/

    char *path = kmalloc(header->h_name_size+1);
    memcpy(path, ptr+offset, header->h_name_size);
    offset+=header->h_name_size;
    root_dir->entries[root_dir->e_index].file = kmalloc(sizeof(struct file_t));
    root_dir->entries[root_dir->e_index].file->name = path;
    root_dir->entries[root_dir->e_index].file->size = header->h_file_size;
    root_dir->entries[root_dir->e_index].file->ptr = ptr+offset;
    root_dir->entries[root_dir->e_index].type = DESC_FILE;
    root_dir->e_index++;

    CLEAR_CHECKPOINT();
}

void carc_parse_entry(char *ptr, struct dir_t *root_dir) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);

    struct carc_ar_t *header = (struct carc_ar_t *)(ptr+offset);
    //log_debug("type: %c\n", header->h_type);
    //log_debug("name: %d\n", header->h_name_size);
    //log_debug("content: %d\n", header->h_file_size);
    offset+=sizeof(struct carc_ar_t);
    switch (header->h_type){
        case CPIO_DIR: {
            carc_parse_dir(ptr, header, root_dir);
            break;
        }
        case CPIO_FILE: {
            carc_parse_file(ptr, header, root_dir);
        }
    }
    offset+=header->h_file_size;

    CLEAR_CHECKPOINT();
}

void read_carc(char *ptr, struct dir_t *root_dir) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    if (memcmp(ptr, "IRFS", 4) != 0) {
        log_critical("Invalid archive signature\n");
        return;
    }
    offset = 4;
    while (ptr[offset] != 0x00) {
        carc_parse_entry(ptr, root_dir);
    }
    CLEAR_CHECKPOINT();
}
