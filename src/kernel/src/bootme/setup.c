#include <arch/arch.h>
#include <logger/log.h>
#include <arch/serial.h>
#include <libm/string.h>
#include <libm/types.h>
#include <cpu/rand/rand.h>
#include <arch/cmos/cmos.h>
#include <mem/heap/heap.h>
#include <mem/pages/paging.h>
#include <mem/frame/pageframe.h>
#include <cpu/gdt/gdt.h>
#include <cpu/idt/idt.h>
#include <disk/ramdisk.h>
#include <cpu/apic/lapic.h>
#include <cpu/apic/ioapic.h>
#include <cpu/pic/pic.h>
#include <acpi/acpi.h>
#include <cpu/syscalls/sysc.h>
#include <cpu/pic/periodic.h>
#include <cpu/pit/pit.h>
#include <taskman/task.h>

#include <drivers/gfx/gfx.h>
#include <drivers/ps2/keyboard/kb.h>

#include <checkpoint/checkpoint.h>

#if defined(__x86_64__)
    #define F_SETUP_KERNEL setup_x86_64
#elif defined(__i386__)
    #define F_SETUP_KERNEL setup_i386
#elif defined(__aarch64__)
    #define F_SETUP_KERNEL setup_aarch64
#elif __riscv_xlen == 64
    #define F_SETUP_KERNEL setup_riscv64
#else
    #error "Unsupported architecture"
#endif


#ifdef __x86_64__
void setup_x86_64() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    setup_RNG();
    setup_paging();
    setup_gdt();
    setup_idt();
    setup_acpi();
    setup_pit();
    setup_lapic();
    setup_pic();
    setup_ioapic();
    setup_periodic();
    //setup_ramdisk();
    CLEAR_CHECKPOINT();
}
#endif


#ifdef i386
void setup_i386() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    setup_RNG();
    CLEAR_CHECKPOINT();
}
#endif

#ifdef __aarch64__
void setup_aarch64() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    setup_RNG();
    CLEAR_CHECKPOINT();
}
#endif

#if __riscv_xlen == 64
void setup_riscv64() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    setup_RNG();
    CLEAR_CHECKPOINT();
}
#endif

void pre_setup() {
    #ifdef __x86_64__
    initialize_serial();
    #endif
    #ifdef __i386__
    initialize_serial();
    #endif
}

void enable_interrupts() {
    #ifdef __x86_64__
    asm volatile("sti");
    #endif
    #ifdef __i386__
    asm volatile("sti");
    #endif
}


void check_syscalls() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    u64 ret;
    __asm__ volatile (
    "movq $0x00, %%rax\n\t"
    "int $0x80"
    : "=a" (ret)
    :
    : "memory"
    );
    log_debug("Syscall test returned: %x\n", ret);
    CLEAR_CHECKPOINT();
}

void process1() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    while (1) {
        log_debug("Process 1 is running\n");
        // Simulate doing some work
        for (volatile int i = 0; i < 1000000; i++);
    }
    CLEAR_CHECKPOINT();
}

void process2() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    while (1) {
        log_debug("Process 2 is running\n");
        // Simulate doing some work
        for (volatile int i = 0; i < 1000000; i++);
    }
    CLEAR_CHECKPOINT();
}

void global_post_setup() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    setup_gfx();
    syscalls_init();
    enable_interrupts();
    check_syscalls();

    log_print("\n\n\n\n\n\n");

    spawn_task(process1);
    //spawn_task(process2);

    taskman_start_schedule();

    CLEAR_CHECKPOINT();
}

void primitive_kernel_setup() {
    pre_setup();
    setup_limine_api();
    F_SETUP_KERNEL();
    global_post_setup();
}
