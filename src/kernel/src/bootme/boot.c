#include <arch/arch.h>

extern void primitive_kernel_setup(void);

void _start(void) {
    /*
    @details: Entry point of the kernel
    */
    primitive_kernel_setup();
    hcf();
}
