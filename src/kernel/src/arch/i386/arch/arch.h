#include <libm/types.h>

#ifndef I386_ARCH_H
#define I386_ARCH_H

void hcf(void);
u64 rand_seed(void);

#endif