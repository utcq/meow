#include <libm/types.h>

#ifndef ARCH_CMOS_H
#define ARCH_CMOS_H

#define CURRENT_YEAR_PREFIX 2000

struct rtc_time {
    u8 second;
    u8 minute;
    u8 hour;
    u8 day;
    u8 month;
    u32 year;
};

struct rtc_time read_rtc();

#endif