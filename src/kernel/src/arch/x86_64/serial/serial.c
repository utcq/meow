#include <arch/serial.h>
#include <libm/types.h>
#include <arch/arch.h>

void initialize_serial() {
    /*
    @details: Standard COM1 (Serial) Init and Test
    */
    outb(COM1 + 1, 0x00);
    outb(COM1 + 3, 0x80);
    outb(COM1 + 0, 0x03);
    outb(COM1 + 1, 0x00);
    outb(COM1 + 3, 0x03);
    outb(COM1 + 2, 0xC7);
    outb(COM1 + 4, 0x0B);
    outb(COM1 + 4, 0x1E);
    outb(COM1 + 0, 0xAE);
    if(inb(COM1 + 0) != 0xAE) {
        // @brief: faulty COM1 port, halt, catch fire
        hcf();
   }
   outb(COM1 + 4, 0x0F);
}

bit b_serial_active() {
    /*
    @details: Standard implementation to check if data has been received
    */
    return inb(COM1 + 5) & 1;
}

bit b_transmit_empty() {
    /*
    @details: Standard implementation to check if the transmit buffer is empty
    */
    return inb(COM1 + 5) & 0x20;
}

char read_serial() {
    /*
    @details: Standard implementation to read data from COM1 port
    */
    while(!b_serial_active());
    return inb(COM1);
}

void write_serial(char data) {
    /*
    @details: Standard implementation to write data to COM1 port
    */
    while (!b_transmit_empty());
    outb(COM1,data);
}

void io_wait(void) {
    /*
    @details: Standard implementation to wait for I/O operation to complete
    */
    __asm__ __volatile__ ("outb %%al, $0x80" :: "a"(0));
}