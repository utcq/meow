#include <libm/types.h>

#ifndef X86_64_ARCH_H
#define X86_64_ARCH_H

void hcf(void);
unsigned long rand_seed(void);
u64 rdmsr(u64 msr);
void wrmsr(u64 msr, u64 value);

#endif // X86_64_ARCH_H