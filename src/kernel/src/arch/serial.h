#ifdef __x86_64
#include <arch/x86_64/serial/serial.h>
#endif
#ifdef __i386__
#include <arch/i386/serial/serial.h>
#endif
#if __riscv_xlen == 64
#include <arch/aarch64/serial/serial.h>
#endif