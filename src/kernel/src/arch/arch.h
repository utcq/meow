#ifdef __x86_64__
#include <arch/x86_64/arch/arch.h>
#endif
#ifdef __i386__
#include <arch/i386/arch/arch.h>
#endif
#ifdef __aarch64__
#include <arch/aarch64/arch/arch.h>
#endif
#if __riscv_xlen == 64
#include <arch/riscv64/arch/arch.h>
#endif