#include <libm/types.h>

#ifndef X86_64_ARCH_H
#define X86_64_ARCH_H

void hcf(void);
unsigned long rand_seed(void);

#endif // X86_64_ARCH_H