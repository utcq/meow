#include <libm/types.h>

#ifndef TASKMAN_TASK_H
#define TASKMAN_TASK_H
#include <cpu/idt/idt.h>


/*
INP: Initialised but not prepared (not ready to schedule)
RTS: Ready to schedule
WFT: Waiting for timer (sleeping)
DBR: Dead but reusable (zombie)
*/

#define TASK_RESERVED_STACK 0x200 // Page dedicated to task

typedef enum {
    TASK_STATE_INP,
    TASK_STATE_RTS,
    TASK_STATE_RUNNING,
    TASK_STATE_WFT,
    TASK_STATE_DBR,
} task_state_t;

typedef struct task_t {
    u32 pid;
    task_state_t state;
    struct irq_frame *ctx;

    struct task_t *next;
    struct task_t *prev;
} task_t;

u32 spawn_task(void (*entry)(void));
void taskman_schedule(struct irq_frame *ctx);
void taskman_start_schedule();

#endif