#include <libm/types.h>
#include <taskman/task.h>
#include <mem/heap/heap.h>
#include <libm/string.h>
#include <logger/log.h>

#include <checkpoint/checkpoint.h>

extern void _hw_ctx_activate(struct irq_frame *ctx);
extern void _hw_ctx_bsave(struct irq_frame *ctx);

task_t *task_queue;
task_t *active_task;

task_t *__allocate_task() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    task_t *task;
    if (!task_queue) {
        task = (task_t *)kmalloc(sizeof(task_t));
        task_queue = task;
    } else {
        task_t *last = task_queue;
        while (last->next && last->state != TASK_STATE_DBR) {
            last = last->next;
        }
        if (last->state == TASK_STATE_DBR) {
            last->state = TASK_STATE_INP;
            CLEAR_CHECKPOINT();
            return last;
        }
        task = (task_t *)kmalloc(sizeof(task_t));
        last->next = task;
        task->prev = last;
        task->pid = last->pid + 1;
        task->state = TASK_STATE_INP;
    }
    CLEAR_CHECKPOINT();
    return task;
}

u32 spawn_task(void (*entry)(void)) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    task_t *task = __allocate_task();
    task->ctx = (struct irq_frame *)kmalloc(sizeof(struct irq_frame));
    task->ctx->return_address = (u64)entry;
    task->ctx->rsp = (u64)kmalloc(TASK_RESERVED_STACK);
    //_hw_ctx_bsave(task->ctx);
    task->state = TASK_STATE_RTS;
    log_debug("Spawned task %d as ctx: %p\n", task->pid, task->ctx);
    CLEAR_CHECKPOINT();
    return task->pid;
}

void switch_tasks(task_t *target, struct irq_frame *ctx) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    log_debug("Switching task\n");

    memcpy(active_task->ctx, ctx, sizeof(struct irq_frame));
    active_task->state = TASK_STATE_RTS;
    target->state = TASK_STATE_RUNNING;
    active_task = target;
    CLEAR_CHECKPOINT();
    _hw_ctx_activate(target->ctx);
}

task_t *__scheduler_resolve_next() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    log_debug("Scheduler algorithm\n");
    task_t *target = active_task->next;
    if (!target) {
        target = task_queue;
    }
    while (target->state != TASK_STATE_RTS) {
        target = target->next;
        if (!target) {
            target = task_queue;
        }
    }
    CLEAR_CHECKPOINT();
    return target;
}

void taskman_schedule(struct irq_frame *ctx) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    log_debug("Scheduling trigger\n");
    task_t *target = __scheduler_resolve_next();
    CLEAR_CHECKPOINT();
    switch_tasks(target, ctx);
}

void taskman_start_schedule() {
    log_debug("Starting scheduler...\n");
    active_task = task_queue;
    if (!active_task) { return; }
    _hw_ctx_activate(active_task->ctx);
}