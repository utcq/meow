#include <libm/types.h>
#include <limapi/api.h>

#ifndef MEM_PAGEFRAME_H
#define MEM_PAGEFRAME_H

#define BLOCK_SIZE 4096
#define BITMAP_SIZE 0x1000

#define VIRTUAL_TO_PHYSICAL(ptr) ((void*)((u64)ptr) - (u64)get_hhdm_mem())
#define PHYSICAL_TO_VIRTUAL(ptr) ((void*)((u64)ptr) + (u64)get_hhdm_mem())

void *alloc_page();
void free_page(void *frame);
u64 get_free_size();
void init_pageframe();

#endif