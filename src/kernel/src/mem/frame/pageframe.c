#include <libm/types.h>
#include <limapi/api.h>
#include <logger/log.h>
#include <libm/string.h>
#include <mem/frame/pageframe.h>
#include <mem/pages/paging.h>

#include <checkpoint/checkpoint.h>

extern u64 _virtual_base;

static u8 *bitmap;
static u64 bitmap_size;
u16 current_frame = 0;
void *first_frame = 0;

void *alloc_page() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    /*
    @details: Allocate, clear and return a page frame
    */
    while (current_frame < bitmap_size && bitmap[current_frame]) {
        current_frame++;
    }
    if (current_frame >= bitmap_size) {
        return NULL;
    }
    bitmap[current_frame] = 1;
    void *allocated_frame = first_frame + (current_frame * BLOCK_SIZE);
    current_frame++;
    memset(allocated_frame, 0, BLOCK_SIZE);
    CLEAR_CHECKPOINT();
    return allocated_frame;
}

void free_page(void *frame) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    /*
    @details: Free a page frame
    */
    u16 frame_index = (frame - first_frame) / BLOCK_SIZE;
    bitmap[frame_index] = 0;
    current_frame = frame_index;
    CLEAR_CHECKPOINT();
}


u64 get_free_size() {
    /*
    @details: Return the amount of free memory
    */
    return get_biggest_memory()->length - (current_frame * BLOCK_SIZE);
}

void init_pageframe() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    /*
    @details: Initialize the page frame allocator
    */
    bitmap = (u8*)PHYSICAL_TO_VIRTUAL(get_2nd_biggest_memory()->base);
    bitmap_size = get_2nd_biggest_memory()->length;
    memset(bitmap, 0, bitmap_size);
    
    first_frame = PHYSICAL_TO_VIRTUAL(get_biggest_memory()->base);
    log_info("First frame: %x\n", first_frame);
    CLEAR_CHECKPOINT();
}
