#include <libm/types.h>

#ifndef MEM_HEAPMAN_H
#define MEM_HEAPMAN_H

#define DIV_ROUND_UP(x, y) (x + (y - 1)) / y
#define PAGE_SIZE 0x1000

struct heap_allocation {
    size_t size;
    void *address;
    struct heap_allocation *prev;
    bit free;
} __attribute__((packed));

struct heap_page {
    size_t free_space;
    size_t used_space;
    struct heap_allocation *latest_allocation;
    void *page;
    struct heap_page *prev;
} __attribute__((packed));

void *kmalloc(size_t size);
void free(void *ptr);
void *realloc(void *ptr, size_t size);

#endif