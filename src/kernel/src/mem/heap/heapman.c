#include <libm/types.h>
#include <mem/heap/heap.h>
#include <mem/frame/pageframe.h>
#include <mem/pages/paging.h>
#include <libm/string.h>
#include <logger/log.h>
#include <limapi/api.h>
#include <libm/assert.h>

#include <checkpoint/checkpoint.h>

struct heap_page *current_heap_page;

void *allocpage_and_handle() {
    void *frame = alloc_page();
    assert(frame != NULL, "Out of memory");
    map_page((u64)frame, (u64)VIRTUAL_TO_PHYSICAL(frame), PAGE_WRITE | PAGE_PRESENT);
    return frame;
}
struct heap_page *alloc_heap_page(size_t size) {
    struct heap_page *page = allocpage_and_handle();
    page->free_space = PAGE_SIZE - sizeof(struct heap_page);
    page->latest_allocation = NULL;
    page->prev = current_heap_page;
    page->page = page + sizeof(struct heap_page);
    u16 additional_pages = DIV_ROUND_UP(size, PAGE_SIZE)-1;
    for (u16 i = 0; i < additional_pages; i++) {
        allocpage_and_handle(); // Reserve pages for the heap
    }
    return page;
}

struct heap_allocation *alloc_heapblock(struct heap_page *parent, size_t size) {
    struct heap_allocation *block = parent->page + parent->used_space;
    block->size = size;
    block->free = 0;
    block->prev = parent->latest_allocation;
    parent->latest_allocation = block;
    block->address = block + sizeof(struct heap_allocation);
    parent->used_space += sizeof(struct heap_allocation) + size;
    return block;
}

struct heap_allocation *block_segmentation(struct heap_page *parent, struct heap_allocation *block, size_t size) {
    if (size+sizeof(struct heap_allocation) >= block->size) {
        block->free = 0;
        return block;
    }
    struct heap_allocation *seg_block = block->address + block->size - size - sizeof(struct heap_allocation);
    seg_block->size = size;
    seg_block->free = 0;
    seg_block->prev = parent->latest_allocation;
    parent->latest_allocation = seg_block;
    seg_block->address = seg_block + sizeof(struct heap_allocation);
    block->size -= size + sizeof(struct heap_allocation);
    return seg_block;
}

void *kmalloc(size_t size) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    struct heap_page *target_page = current_heap_page;
    while (target_page != NULL && target_page->free_space < size) {
        target_page = target_page->prev;
    }
    if (target_page == NULL) {
        target_page = alloc_heap_page(size);
        current_heap_page = target_page;
    }
    struct heap_allocation *target_block = target_page->latest_allocation;
    while (target_block != NULL && (target_block->free == 0 || target_block->size < size)) {
        target_block = target_block->prev;
    }
    if (target_block == NULL) {
        target_block = alloc_heapblock(target_page, size);
    } else {
        target_block = block_segmentation(target_page, target_block, size);
    }
    CLEAR_CHECKPOINT();
    return target_block->address;
}

void check_and_freepage(struct heap_page *page) {
    struct heap_allocation *block = page->latest_allocation;
    while (block != NULL) {
        if (block->free == 0) {
            return;
        }
        block = block->prev;
    }
    u16 additional_pages = DIV_ROUND_UP(page->used_space+page->free_space, PAGE_SIZE)-1;
    free_page(page);
    for (u16 i = 0; i < additional_pages; i++) {
        free_page(page + (i+1)*PAGE_SIZE);
    }
}

void free(void *ptr) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    struct heap_page *target_page = current_heap_page;
    while (target_page != NULL) {
        struct heap_allocation *target_block = target_page->latest_allocation;
        while (target_block != NULL) {
            if (target_block->address == ptr) {
                target_block->free = 1;
                check_and_freepage(target_page);
                CLEAR_CHECKPOINT();
                return;
            }
            target_block = target_block->prev;
        }
        target_page = target_page->prev;
    }
    CLEAR_CHECKPOINT();
}

void *realloc(void *ptr, size_t size) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    void *newptr = kmalloc(size);
    struct heap_page *target_page = current_heap_page;
    while (target_page != NULL) {
        struct heap_allocation *target_block = target_page->latest_allocation;
        while (target_block != NULL) {
            if (target_block->address == ptr) {
                memcpy(newptr, ptr, target_block->size);
                free(ptr);
                CLEAR_CHECKPOINT();
                return newptr;
            }
            target_block = target_block->prev;
        }
        target_page = target_page->prev;
    }
    CLEAR_CHECKPOINT();
    return NULL;
}
