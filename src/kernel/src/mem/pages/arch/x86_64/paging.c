#include <mem/pages/paging.h>
#include <mem/frame/pageframe.h>
#include <libm/types.h>
#include <logger/log.h>
#include <limapi/api.h>
#include <libm/string.h>
#include <mem/pages/paging.h>

#include <checkpoint/checkpoint.h>

extern struct limine_kernel_address_request kernaddress_req;
extern struct limine_memmap_request memmap;

extern void** _text_start_ld;
extern void** _text_end_ld;
extern void** _rodata_start_ld;
extern void** _rodata_end_ld;
extern void** _data_start_ld;
extern void** _data_end_ld;

static page_table_t *pml4;

void tlb_flush() {
    /*
    @details: Self explanatory
    */
    asm volatile("mov %cr3, %rax; mov %rax, %cr3");
}

void load_cr3( void* cr3_value ) {
    /*
    @details: Self explanatory
    */
    cr3_value = VIRTUAL_TO_PHYSICAL(cr3_value);
    asm volatile("mov %0, %%cr3" :: "r"((u64)cr3_value) : "memory");
}

void enable_pae() {
    /*
    @details: Self explanatory
    */
    u64 cr4;
    asm volatile("mov %%cr4, %0" : "=r"(cr4));
    cr4 |= 0x20;
    asm volatile("mov %0, %%cr4" :: "r"(cr4));
}

void enable_paging() {
    /*
    @details: Self explanatory
    */
    enable_pae();
    u64 cr0;
    asm volatile("mov %%cr0, %0" : "=r"(cr0));
    cr0 |= 0x80000000;
    asm volatile("mov %0, %%cr0" :: "r"(cr0));
    log_success("Paging enabled\n");
}

void map_page(u64 vaddr, u64 physical_addr, u64 flags) {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    /*
    @details: Map virtual address to physical address + flags [4 levels of paging]
              Flush tlb if page is already present (changed permissions)
    */
    u16 pml4_index = (vaddr >> 39) & 0x1ff;
    u16 pml3_index = (vaddr >> 30) & 0x1ff;
    u16 pml2_index = (vaddr >> 21) & 0x1ff;
    u16 pml1_index = (vaddr >> 12) & 0x1ff;
    u64 *pml3 = NULL;
    if (pml4[pml4_index] & PAGE_PRESENT) {
        pml3 = PHYSICAL_TO_VIRTUAL(PTE_GET_ADDR(pml4[pml4_index]));
    } else {
        pml3 = alloc_page();
        pml4[pml4_index] = (u64)VIRTUAL_TO_PHYSICAL(pml3) | PAGE_PRESENT | PAGE_WRITE;
    }
    u64 *pml2 = NULL;
    if (pml3[pml3_index] & PAGE_PRESENT) {
        pml2 = PHYSICAL_TO_VIRTUAL(PTE_GET_ADDR(pml3[pml3_index]));
    } else {
        pml2 = alloc_page();
        pml3[pml3_index] = (u64)VIRTUAL_TO_PHYSICAL(pml2) | PAGE_PRESENT | PAGE_WRITE;
    }

    u64 *pml1 = NULL;
    if (pml2[pml2_index] & PAGE_PRESENT) {
        pml1 = PHYSICAL_TO_VIRTUAL(PTE_GET_ADDR(pml2[pml2_index]));
    } else {
        pml1 = alloc_page();
        pml2[pml2_index] = (u64)VIRTUAL_TO_PHYSICAL(pml1) | PAGE_PRESENT | PAGE_WRITE;
    }
    bit perm_mod = pml1[pml1_index] & PAGE_PRESENT;
    pml1[pml1_index] = physical_addr | flags;
    if (perm_mod) { tlb_flush(); }
    CLEAR_CHECKPOINT();
}

void map_pages(u64 vaddr, u64 physical_addr, size_t size, u64 flags) {
    /*
    @details: Map multiple pages
    */
    for (u64 i = 0; i < size; i += PAGE_SIZE) {
        map_page(vaddr + i, physical_addr + i, flags);
    }
}


void map_kernel_code() {
    /*
    @details: Map sections of the kernel the correct physical addresses
    */
    u64 phys_base = kernaddress_req.response->physical_base;
    u64 virt_base = kernaddress_req.response->virtual_base;

    u64 text_start = ALIGN_DOWN((u64)&_text_start_ld, PAGE_SIZE);
    u64 text_end = ALIGN_UP((u64)&_text_end_ld, PAGE_SIZE);
    u64 rodata_start = ALIGN_DOWN((u64)&_rodata_start_ld, PAGE_SIZE);
    u64 rodata_end = ALIGN_UP((u64)&_rodata_end_ld, PAGE_SIZE);
    u64 data_start = ALIGN_DOWN((u64)&_data_start_ld, PAGE_SIZE);
    u64 data_end = ALIGN_UP((u64)&_data_end_ld, PAGE_SIZE);

    //log_info("Text start: %x, Text end: %x\n", text_start, text_end);
    //log_info("Rodata start: %x, Rodata end: %x\n", rodata_start, rodata_end);
    //log_info("Data start: %x, Data end: %x\n", data_start, data_end);

    log_running("Mapping kernel\n");

    for (u64 text = text_start; text < text_end; text += PAGE_SIZE) {
        log_info("Mapping memory: %x to %x\n", text, text - virt_base + phys_base);
        map_page(text, text - virt_base + phys_base, PAGE_PRESENT | PAGE_USER);
    }
    for (u64 rodata = rodata_start; rodata < rodata_end; rodata += PAGE_SIZE) {
        log_info("Mapping memory: %x to %x\n", rodata, rodata - virt_base + phys_base);
        map_page(rodata, rodata - virt_base + phys_base, PAGE_PRESENT | PAGE_NX | PAGE_USER);
    }
    for (u64 data = data_start; data < data_end; data += PAGE_SIZE) {
        log_info("Mapping memory: %x to %x\n", data, data - virt_base + phys_base);
        map_page(data, data - virt_base + phys_base, PAGE_PRESENT | PAGE_WRITE | PAGE_NX | PAGE_USER);
    }
}

void map_found_memory() {
    /*
    @details: Map limine found memory to virtual memory
    */
    log_running("Mapping found memory\n");
    for (size_t i =0; i < memmap.response->entry_count; i++) {
        struct limine_memmap_entry *entry = memmap.response->entries[i];
        u64 vaddr = (u64)PHYSICAL_TO_VIRTUAL(entry->base);
        u64 paddr = entry->base;
        u64 pages = entry->length / PAGE_SIZE;
        if (entry->type != LIMINE_MEMMAP_USABLE) {
            continue;
        }
        log_info("Mapping memory: %x to %x [=%d PAGES]\n", vaddr, paddr, pages);
        for (u64 page = 0; page < pages; page++) {
            map_page(vaddr + (page * PAGE_SIZE), paddr + (page * PAGE_SIZE), PAGE_PRESENT | PAGE_WRITE | PAGE_USER);
        }
    }
}

void map_hhdm_memory() {
    /*
    @details: Map HHDM memory to virtual memory (no identity)
    */
    u64 size = 0;
    for (u8 i=0; i<memmap.response->entry_count; i++) {
        struct limine_memmap_entry *entry = memmap.response->entries[i];
        if (entry->type != LIMINE_MEMMAP_RESERVED) {
            size += entry->length;
        }
    }
    size = 0x100000000-size;
    u64 base = (u64)get_hhdm_mem();
    log_running("Mapping HHDM memory\n");
    log_info("Mapping memory: from %x to %x\n", base, base + size);
    for (u64 i = 0; i < size; i += PAGE_SIZE) {
        map_page(base + i, i, PAGE_PRESENT | PAGE_WRITE | PAGE_NX | PAGE_USER);
    }
}

void map_bootloader_memory() {
    /*
    @details: Map bootloader reclaimable memory
    */
    log_running("Mapping bootloader memory\n");
    for (u8 i=0; i<memmap.response->entry_count; i++) {
        struct limine_memmap_entry *entry = memmap.response->entries[i];
        if (entry->type == LIMINE_MEMMAP_BOOTLOADER_RECLAIMABLE) {
            u64 vaddr = (u64)PHYSICAL_TO_VIRTUAL(entry->base);
            u64 paddr = entry->base;
            u64 pages = entry->length / PAGE_SIZE;
            log_info("Mapping memory: %x to %x [=%d PAGES]\n", vaddr, paddr, pages);
            for (u64 page = 0; page < pages; page++) {
                map_page(vaddr + (page * PAGE_SIZE), paddr + (page * PAGE_SIZE), PAGE_PRESENT | PAGE_WRITE | PAGE_USER);
            }
        }
    
    }
}

void initial_mapping() {
    map_bootloader_memory();
    map_found_memory();
    map_hhdm_memory();
    map_kernel_code();
}

void setup_paging() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);
    init_pageframe();
    pml4 = (page_table_t*)alloc_page();
    initial_mapping();
    load_cr3(pml4);
    enable_paging();
    CLEAR_CHECKPOINT();
}