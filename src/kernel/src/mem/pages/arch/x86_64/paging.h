#include <libm/types.h>

#ifndef MEM_PAGING_X86_64_H
#define MEM_PAGING_X86_64_H

#include <mem/frame/pageframe.h>

#define ROUND_PAGE_DOWN(x) ((x) & ~(0x1000 - 1))


#define PTE_ADDR_MASK 0x000ffffffffff000
#define PTE_GET_ADDR(VALUE) ((VALUE) & PTE_ADDR_MASK)
#define PTE_GET_FLAGS(VALUE) ((VALUE) & ~PTE_ADDR_MASK)
#define DIV_ROUND_UP(x, y) (x + (y - 1)) / y
#define ALIGN_UP(x, y) DIV_ROUND_UP(x, y) * y
#define ALIGN_DOWN(x, y) (x / y) * y

typedef u64 page_table_t;

#define PAGE_SIZE 0x1000

#define PAGE_PRESENT 1ull
#define PAGE_WRITE (1ull << 1)
#define PAGE_USER (1ull << 2)
#define PAGE_UNCACHEABLE (1ull << 4)
#define PAGE_NX (1ull << 63)

void setup_paging();
void map_page(u64 virtual_addr, u64 physical_addr, u64 flags);
void map_pages(u64 virtual_addr, u64 physical_addr, size_t size, u64 flags);

#endif