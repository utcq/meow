#include <logger/log.h>
#include <libm/types.h>
#include <arch/arch.h>

void assert(bit condition, const char *message) {
    /*
    @details: If the condition is false, log a failed assertion message. Half, Catch fire
    */
    if (!condition) {
        log_failed_assertion(message);
        hcf();
        // TODO: Kernel panic
    }
}