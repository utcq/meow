#ifndef LIBM_ASSERT_H
#define LIBM_ASSERT_H

void assert(unsigned condition, const char *message);

#endif