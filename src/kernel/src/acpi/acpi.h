#include <libm/types.h>

#ifndef APCI_APCI_H
#define APCI_APCI_H

struct RSDP1 {
    char Signature[8];
    u8 Checksum;
    char OEMID[6];
    u8 Revision;
    u32 RsdtAddress;
} __attribute__ ((packed));

struct XSDP_t {
    char Signature[8];
    u8 Checksum;
    char OEMID[6];
    u8 Revision;
    u32 RsdtAddress;      // deprecated since version 2.0

    u32 Length;
    u64 XsdtAddress;
    u8 ExtendedChecksum;
    u8 reserved[3];
} __attribute__ ((packed));

struct ACPISDTHeader {
    char Signature[4];
    u32 Length;
    u8 Revision;
    u8 Checksum;
    char OEMID[6];
    char OEMTableID[8];
    u32 OEMRevision;
    u32 CreatorID;
    u32 CreatorRevision;
}; __attribute__ ((packed))

struct RSDT {
    struct ACPISDTHeader h;
    u32 sdtAddresses[];
} __attribute__ ((packed));

struct MADT_Base {
    u8 type;
    u8 length;
} __attribute__ ((packed));

struct MADT_PLAPIC {
    u8 acpi_processor_id;
    u8 apic_id;
    u32 flags;
} __attribute__ ((packed));

struct MADT_IOAPIC {
    u8 ioapic_id;
    u8 reserved;
    u32 ioapic_address;
    u32 global_system_interrupt_base;
} __attribute__ ((packed));

struct MADT_ISO {
    u8 bus_source;
    u8 irq_source;
    u32 global_system_interrupt;
    u16 flags;
} __attribute__ ((packed));

struct MADT_NMI {
    u8 nmi_source;
    u8 reserved;
    u16 flags;
    u32 global_system_interrupt;
} __attribute__ ((packed));

struct MADT_LNMI {
    u8 acpi_processor_id;
    u16 flags;
    u8 lint;
} __attribute__ ((packed));

struct MADT_AO {
    u16 reserved;
    u64 lapic_address;
} __attribute__ ((packed));

struct MADT_Entry {
    struct MADT_Base b;
    union {
        struct MADT_PLAPIC  *p;
        struct MADT_IOAPIC  *i;
        struct MADT_ISO     *s;
        struct MADT_NMI     *n;
        struct MADT_LNMI    *l;
        struct MADT_AO      *o;
    };
} __attribute__ ((packed));

struct MADT {
    struct ACPISDTHeader h;
    u32 LAPICAddress;
    u32 Flags;
    struct MADT_Entry entries[16];
} __attribute__ ((packed));

struct MADT_Map {
    u8 entries_count;
    struct MADT_Entry entries[16];
};


void setup_acpi();
struct MADT_IOAPIC *acpi_madt_get_ioapic();
u64 get_acpi_lapic_address();

#endif
