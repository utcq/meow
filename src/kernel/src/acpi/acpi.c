#include <libm/types.h>
#include <acpi/acpi.h>
#include <limapi/api.h>
#include <logger/log.h>
#include <libm/string.h>
#include <libm/assert.h>
#include <mem/pages/paging.h>

#include <checkpoint/checkpoint.h>

struct RSDT *rsdt;
struct MADT *madt;

struct MADT *acpi_get_madt(){
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);

    u8 entries = (rsdt->h.Length - sizeof(rsdt->h))/4;
    for (u8 i=0;i<entries;i++) {
        u64 address = (u64)PHYSICAL_TO_VIRTUAL(rsdt->sdtAddresses[i]);
        map_page(address, rsdt->sdtAddresses[i], PAGE_PRESENT | PAGE_WRITE);
        struct ACPISDTHeader *h = (struct ACPISDTHeader*) address;
        if (memcmp(h->Signature, "APIC", 4) == 0) {
            CLEAR_CHECKPOINT();
            return (struct MADT *)h;
        }
    }

    CLEAR_CHECKPOINT();
    return NULL;
}

struct MADT_IOAPIC *acpi_madt_get_ioapic() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);

    struct MADT_Entry *entry = (madt->entries);
    struct MADT_IOAPIC *ioapic;
    while ((u64)entry < (u64)madt + madt->h.Length) {
        switch (entry->b.type) {
            case 0: {
                //struct MADT_PLAPIC *plapic = (struct MADT_PLAPIC *)entry+sizeof(struct MADT_Base);
                //log_info("Processor Local APIC: %d %d %d\n", plapic->acpi_processor_id, plapic->apic_id, plapic->flags);
                break;
            }
            case 1: {
                ioapic = (struct MADT_IOAPIC *)entry+sizeof(struct MADT_Base);
                //log_info("IO APIC: %d %d %p\n", ioapic->ioapic_id, ioapic->reserved, ioapic->ioapic_address);
                break;
            }
            case 2: {
                //struct MADT_ISO *iso = (struct MADT_ISO *)entry+sizeof(struct MADT_Base);
                //log_info("Interrupt Source Override: %d %d\n", iso->irq_source, iso->bus_source);
                break;
            }
            case 3: {
                //struct MADT_NMI *nmi = (struct MADT_NMI*)entry+sizeof(struct MADT_Base);
                //log_info("NMI: %d %p\n", nmi->nmi_source, nmi->global_system_interrupt);
                break;
            }
            case 4: {
                //struct MADT_LNMI *lnmi = (struct MADT_LNMI*)entry+sizeof(struct MADT_Base);
                //log_info("Local APIC NMI: %d %d\n", lnmi->acpi_processor_id, lnmi->lint);
                break;
            }
            default:
                //log_info("Unknown MADT entry type: %d\n", entry->b.type);
                break;
        }
        entry = (struct MADT_Entry *)((u64)entry + entry->b.length);
    }
    CLEAR_CHECKPOINT();
    return ioapic;
}


void setup_acpi() {
    CHECKPOINT(__FILE__, __PRETTY_FUNCTION__);

    struct RSDP1 *rsdp1 = (struct RSDP1 *)get_rsdp_address();

    assert(memcmp(rsdp1->Signature, "RSD PTR ", 8) == 0, "RSDP1 signature mismatch\n");
    assert(rsdp1->Revision == 0, "RSDP Version 2.0 not supported\n");
    unsigned char checksum = 0;
    for (u32 i = 0; i < sizeof(struct RSDP1); i++) { checksum += ((u8 *)rsdp1)[i]; }
    assert((checksum & 0xFF) == 0, "RSDP1 checksum invalid\n");
    u64 rsdt_address = (u64)PHYSICAL_TO_VIRTUAL(rsdp1->RsdtAddress);
    map_page(rsdt_address, rsdp1->RsdtAddress, PAGE_PRESENT);

    rsdt = (struct RSDT *)rsdt_address;
    assert(memcmp(rsdt->h.Signature, "RSDT", 4) == 0, "RSDT signature mismatch\n");
    checksum = 0;
    for (u32 i=0;i<rsdt->h.Length;i++) { checksum += ((char *)rsdt)[i]; }
    assert((checksum & 0xFF) == 0, "RSDT checksum invalid\n");

    madt = acpi_get_madt();
    assert(madt != NULL, "MADT not found\n");

    CLEAR_CHECKPOINT();
}

u64 get_acpi_lapic_address() {
    return madt->LAPICAddress;
}