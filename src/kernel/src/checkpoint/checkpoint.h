#ifndef CHECKPOINT_CHECK_H
#define CHECKPOINT_CHECK_H

#define CHECKPOINT(path, func) save_checkpoint(path, func)
#define CLEAR_CHECKPOINT() clear_checkpoint()

struct checkpoint {
    char *source;
    const char *function;
};

void save_checkpoint(char *source, const char *function);
void clear_checkpoint();

#endif