#include <checkpoint/checkpoint.h>
#include <libm/types.h>
#include <logger/log.h>
#include <libm/string.h>

struct checkpoint checkpoint_source;
struct checkpoint checkpoint_list[100];
u32 checkp_index=0;

void save_checkpoint(char *source, const char *function) {
    checkpoint_source = (struct checkpoint){source,function};
    checkp_index++;
    checkpoint_list[checkp_index] = checkpoint_source;
}

void clear_checkpoint() {
    if (checkp_index == 0) {
        return;
    }
    checkp_index--;
    checkpoint_source = checkpoint_list[checkp_index];
}